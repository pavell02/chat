const prompt = require('prompt')
const fs 	 = require('fs')

prompt.start()

function ucfirst( str ) {
   	let f = str.charAt(0).toUpperCase();
   	return f + str.substr(1, str.length-1);
}

prompt.get(['path'], function (err, {path}) {
	path = path.replace(/^frontend\//,'./')
	let vue = path.match(/\/([a-zA-Z0-1-]+)$/)
	console.log(vue)
	vue = vue[1].split('-')
	for(let i in vue) {
		vue[i] = ucfirst(vue[i])
	}
	vue = vue.join('')
	let files = [
		`${vue}.vue`,
		'script.js',
		'template.html',
		'style.scss',
	]
	let contentVue = `<template src="./template.html"></template>
<style scoped src="./style.scss" lang="scss"></style>
<script src="./script.js"></script>`
	let contentScript = `export default {

}`
	let content = [
		contentVue,
		contentScript,
	]
	for(let index in files) {
		let fileContent = content[index] || ''
		fs.writeFile(`${path}/${files[index]}`, fileContent, (err) => {
	    	if (err) throw err
	    	console.log(`The file "${files[index]}" was succesfully created!`);
		})	
	}
})