import Vue from 'vue'
export default {
	props: ['struct'],
	methods: {
		setValue(index, value) {
			Vue.set(this.struct, index, {...this.struct[index], value: value.trim()})
			this.struct[index].afterSettingValue()
		},
	}
}