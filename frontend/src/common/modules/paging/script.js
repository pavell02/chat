import {positionScroll} from '../../libs/html'
export const PAGE_LIMIT = 25
export default {
    props: ['query', 'data', 'count', 'setCount'],
    methods: {
        scrollTo(e) {
            let pos = positionScroll(e, {bottom: 5})
            if(pos == 'bottom' && this.data.length < this.count) {
                console.log(pos)
                this.loadData()
            }
        },
        loadData() {
            let page = Math.floor(this.data.length / PAGE_LIMIT),
            opt = {
                skip: page * PAGE_LIMIT,
                limit: PAGE_LIMIT,
            }

            this.query(opt, true)
            .then(res => res.json())
            .then(data => {
                    this.data.push(...data.data)
                    this.setCount(data.count)
                    return data    
                }
            )

        }
    },
    created() {
		this.loadData()
    }
}