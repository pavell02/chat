export default function replaceHtmlFromMessage(preparedTextMessage) {
	let tempElem = document.createElement('div')
	tempElem.innerHTML 	= preparedTextMessage
	let imgs 			= tempElem.querySelectorAll('img')
	let outerHTMLImgs 	= []
	let emojis 			= []
	imgs.forEach(el => {
		if(!el.dataset.emojis) return
		outerHTMLImgs.push(el.outerHTML)
		emojis.push(el.dataset.emojis)
	})
	for(let i in outerHTMLImgs) {
		preparedTextMessage = preparedTextMessage.replace(outerHTMLImgs[i], emojis[i])
	}
	preparedTextMessage = preparedTextMessage.replace(/<br>/g,'\n')
	tempElem.innerHTML  = preparedTextMessage
	preparedTextMessage = tempElem.textContent
	preparedTextMessage = preparedTextMessage.replace(/^\s+/,'')
	preparedTextMessage = preparedTextMessage.replace(/\s+$/,'')
	return preparedTextMessage
}
export function returnHtml(text) {
	text = text.replace(/\n/g,'<br>')
	let matches = text.match(/(:.{6})/g)
	for(let key in matches) {
		let smileHTMl = this.$refs.smiles ? this.$refs.smiles.$el.querySelector(`img[data-emojis="${matches[key]}"]`) : false
		if(!smileHTMl) continue
		smileHTMl = smileHTMl.cloneNode(false)
		delete smileHTMl.dataset.emojis
		text = text.replace(matches[key], smileHTMl.outerHTML)
	}
	return text
}
export function positionScroll(event, opt = {bottom: 0}) {
	let el = event.target  || event
	let pos = ''
	if(el.scrollTop  == 0 && el.scrollHeight > el.clientHeight) {
		pos = 'top'
	}
	else if(!opt.bottom ? 
		el.scrollHeight - el.clientHeight == Math.round(el.scrollTop + opt.bottom)
		:
		el.scrollHeight - el.clientHeight > Math.floor(el.scrollTop) 
		&&
		el.scrollHeight - el.clientHeight - opt.bottom < Math.floor(el.scrollTop) 
		) {
		pos = 'bottom'	
	}
	else {
		pos = 'middle'
	}
	return pos
}