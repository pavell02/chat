import Vue from 'vue'
export function checkStruct(struct) {
	let check = true
	for(let i in struct) {
		let checkEach = true
		//check that struct is not empty
		if(struct[i].empty === false) {
			if(struct[i].value == '') {
				Vue.set(struct[i], 'error_message', 'Поле не может быть пустым')
				check = checkEach = false
			}
		}
		//check that fields are same
		if(struct[i].same && checkEach) {
			for(let index in struct) {
				if(i != index && struct[index].same == struct[i].same) {
					if(struct[i].value != struct[index].value) {
						Vue.set(struct[i], 'error_message', 'Значения полей не совподают')
						check = checkEach = false
					}
				}
			}
		}
		//check regexp
		if(struct[i].regularExpresion && !struct[i].regularExpresion.regexp.test(struct[i].value) && checkEach) {
			Vue.set(struct[i], 'error_message', !struct[i].regularExpresion.error_message ? 
				('Допустимые символы: ' + (struct[i].regularExpresion.signs ? struct[i].regularExpresion.signs : 'A-Z a-z 0-9')) :
				struct[i].regularExpresion.error_message)
			check = checkEach = false
		}
		//check min length
		if(struct[i].min && struct[i].value.length < struct[i].min && checkEach) {
			Vue.set(struct[i], 'error_message', 'Значение может быть не меньше:' + struct[i].min)
			check = checkEach = false
		}
		//check max length
		if(struct[i].max && struct[i].value.length > struct[i].max && checkEach) {
			Vue.set(struct[i], 'error_message', 'Значение не может быть больше:' + struct[i].max)
			check = checkEach = false
		}
		//own checking of a field
		check = struct[i].check ? struct[i].check() : check
		checkEach = struct[i].check ? check : checkEach
		if(checkEach) {
			Vue.set(struct[i], 'error_message', undefined)
		}
	}
	return check
}

export function structToFormData(struct) {
	let data = {}
	for(let i in struct) {
		if(struct[i].name) {
			data[struct[i].name] = struct[i].value
		}
	}
	return data
}