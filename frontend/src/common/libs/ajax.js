export default function Ajax(url, opts ) {
	//get csrf token
	let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
	opts = opts || {}
	
	//set correct url
	url = opts.method == 'GET' || opts.method === undefined ? url + dataIntoUrl(opts.data): url
	
	//set files
	let formData = new FormData()
	for(let key in opts.files) {
		formData.append(key, opts.files[key])
	}
	//set body
	for(let key in opts.data) {
		
		formData.append(key, opts.data[key])
	}
	//body of req
	let body = opts.method == 'GET' || opts.method === undefined ?
	{}
	:
	{
		body: opts.files? formData : JSON.stringify(opts.data || '')
	}

	//set headers
	let headers = {
		'CSRF-Token': token, // <-- is the csrf token as a header
		'Content-Type':  'application/json;charset=utf-8',
		...opts.headers,
	}
	if(opts.files) {
		delete headers['Content-Type']
	}	
	
	return fetch(url, {
		credentials: 'same-origin', // <-- includes cookies in the request
		headers,
		method: opts.method || 'GET',
		...body,
	})
}

export function dataIntoUrl(data) {
	let str = '?'
	let i = 0
	for(let key in data) {
		str += `${i != 0 ? '&' : ''}${key}=${data[key]}`
		i++
	}
	return str
}