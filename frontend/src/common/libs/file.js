import Ajax from './ajax'
export function convertImageToBuffer(file) {
	return new Promise((res, rej) => {
		let reader = new FileReader()
		reader.readAsArrayBuffer(file)
		reader.onload = () => res(reader.result)
		reader.onerror = () => rej(reader.error)
	}) 
}
export function sendFile(file, to, type, data) {
	let opts = {
		method: 'POST',
		files: {
			image: file,
		},
		data
	}
	return Ajax(`/updload/${to}/${type}`, opts)
}