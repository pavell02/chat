import HeaderContent from '../components/header-content/HeaderContent.vue'
import Nav from '../components/nav/Nav.vue'
import Auth from '../components/auth/Auth.vue'
import AlertMessages from '../components/alert-messages/AlertMessages.vue'
import {mapState, mapMutations} from 'vuex'

export default {
	name: 'app',
	components:{
		HeaderContent,
		Auth,
		Nav,
		AlertMessages,
	},
	computed: {
		...mapState({
			authView:  state => state.auth.view,
			auth: state => state.auth.auth,
			navMobileView: state => state.nav.navMobileView,
		}),
	},
	methods: {
		...mapMutations(['setMobileView']),
		...mapMutations('nav', ['toggleNavMobileView']),
	},
	beforeCreate() {
		if(this.$route.path == '/alert-message') {
			let message = this.$route.query.message
			if(message) {
				this.$store.commit('alert/addAlertMessage', {message})
			}
			this.$router.push(this.$route.query.path || '/' )
		}
	},
	created() {
		this.setMobileView()
		window.addEventListener('resize', this.setMobileView)
	}
}