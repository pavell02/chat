import Vue from 'vue'
export const AMOUNT_OF_SHOW_ELEMNT = 3
export default () => ({
    namespaced: true,
	state: () => ({
		alertMessages: [],
	}),
	mutations: {
		addAlertMessage(state, message) {
			state.alertMessages.push(message) 
		},
		deleteAlertMessage(state, index) {
			state.alertMessages.splice(index, 1)
		},
		setAlertMessageTimeout(state, timeout) {
			let alerts = state.alertMessages
			if(alerts[0] && (alerts[0].timeout === undefined || alerts[0].timeout === false)) {
				Vue.set(alerts[0], 'timeout', timeout )				
			}
		},
		unsetAlertMessageTimeout(state) {
			clearTimeout(state.alertMessages[0].timeout)
			Vue.set(state.alertMessages[0], 'timeout', false)
		},	
	},
	getters: {
		getAlertMessages: state => {
			let arr = []
			for(let i = 0; i < AMOUNT_OF_SHOW_ELEMNT; i++) {
				if(state.alertMessages[i] !== undefined) {
					arr.push(state.alertMessages[i])					
				}
			}
			return arr
		},
	}
})