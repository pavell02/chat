//import Io   from 'socket.io-client'
import io from './BaseSocket'
export default {
    namespaced: true,
    modules: {
        io,
    },
	state: () => ({
	}),
	mutations: {
    },
    actions: {
        socketConnect() {
            this._io.notification.connect()
        },
        socketDisconnect() {
            this._io.notification.close()
        }
    }
}