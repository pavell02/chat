import Vue from 'vue'
import Ajax from '../common/libs/ajax.js'

export default () => ({
    namespaced: true,
	state: () => ({
		nav: [
			{
				to: '/',
				title: 'Чат',
				name: 'default',
			},
			{
				to: '/friends?filter=REQUESTED&search=',
				title: 'Друзья',
				name: 'friend',
			},
			{
				to: '/conversation',
				title: 'Сообщения',
				name: 'conversation',
			}
		],
		navMobileView: false,
		count: {},
	}),
	mutations: {
		setCounter(state, {name, count}) {
			Vue.set(state.count, name, count)
		},
		getCounter(state, count = false) {
			count = count ? [count] : ['friend', 'conversation']
			for(let name of count){
				let path = '/count/' + name
				Ajax(path)
				.then(res => res.json())
				.then(({count}) => {
					this.commit('nav/setCounter', {name, count})
				})
				.catch(err => console.error(err))
			}
		},
		toggleNavMobileView(state, view) {
			Vue.set(state, 'navMobileView', typeof view !== "boolean" ? !state.navMobileView : view)
		}
	},
	getters: {
		getNav: state => {
			let count = state.count
			let nav = state.nav.map(el => {
				el.count = count[el.name]
				return el
			})
			return nav
		}
	}
})