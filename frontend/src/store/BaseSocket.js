export default {
	state: () => ({
        connected: false,
	}),
	mutations: {
        setConnected(state, v) {
            state.connected = v
        },
    },
    getters: {
        connected(state) {
            return state.connected
        }
    },
    modules: []
}