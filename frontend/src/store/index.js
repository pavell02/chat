import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './Auth.js'
import chat from './Chat.js'
import Alert from './Alert.js'
import Nav from './Nav.js'
import conversation from './Conversation'
import notification from './Notification'

import CommonMessages from './plugin/CommonMessages.js'
import AuthCheck from './plugin/AuthCheck.js'
import AddIO from './plugin/AddIO.js'
import ConversationSocketPlugin from './plugin/ConversationSocketPlugin'
import ChatSocketPlugin from './plugin/ChatSocketPlugin'
import NotificationPlugin from './plugin/NotificationSocket'
import BaseSocketPlugin from './plugin/BaseSocketPlugin'
const ChatCommonMessages = CommonMessages('chat')
const ConversationCommonMessages = CommonMessages('conversation')
const ConversationSocket = ConversationSocketPlugin('conversation')
const ChatSocket = ChatSocketPlugin('chat')
const Notification = NotificationPlugin('notification')
const BaseSocket = BaseSocketPlugin(['notification', 'chat', 'conversation'])


let nav = Nav()
let auth = Auth()
let alert = Alert()
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
	auth,
	chat,
	alert,
	nav,
	conversation,
	notification,
  },
  state:() => ({
		mobileView: false,
  }),
  mutations: {
	setMobileView(state) {
		if(screen.width <= 595) {
			Vue.set(state, 'mobileView', true)
		}
		else {
			Vue.set(state, 'mobileView', false)
		}
	}
  },
  plugins: [
		AddIO,
		BaseSocket,
		ChatCommonMessages,
		ConversationCommonMessages,
		AuthCheck,
		ConversationSocket,
		ChatSocket,
		Notification,
	],
})
export default store