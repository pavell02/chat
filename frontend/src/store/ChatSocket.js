import Vue from 'vue'
import baseSocket from './BaseSocket'

export default namespace => ({
	state: () => ({
        lastAddedMessagesWhere: 1, 
        messages: [],
        waitingMessages: false,
        lengthMessages: 0,
	}),
	mutations: {
        pushMessage(state, {message, where}) {
            state.lastAddedMessagesWhere = where
            if(where == 1) {
                    state.messages.push(message)
            }
            else if(where == -1) {
                state.messages.unshift(message)
            }
        },
        toggleWaitingMessages(state) {
            state.waitingMessages = !state.waitingMessages
        },
        setLengthMessages(state, lenMes)  {
            state.lengthMessages  = lenMes
        },
        setMessages(state, mes) {
            state.messages = []
            state.messages = mes
        },
        setMessagesAsRead(state, ids) {
            for(let i in state.messages) {
                if(ids.includes(state.messages[i].i)) {
                    Vue.set(state.messages[i], 'read', true)
                }
            }
        },
    },
    getters: {
        messagesGetter(state) {
            return state.messages
        }
    },
    actions: {
        emitMessage(ctx, text) {
            if(this.getters[namespace + '/checkBeforeSend']) return
            let obj = {
                text: text,
                type: 'text',
            }
            this.commit(namespace + '/setTextMessage', '')
            this._io[namespace].emit('MESSAGE', obj)
        },
        emitReturnToData() {
            this._io[namespace].emit('RETURN_TO_DATA')
        },
        emitMessagesAreRead(ctx) {
            let ids = []
            let messages = ctx.state.messages
            for(let i in messages) {
                if(!messages[i].read && !messages[i].yours) {
                    ids.push(messages[i].i)
                }
            }
            if(ids.length) this._io[namespace].emit('MESSAGES_ARE_READ', ids)
        },
        emitGetNewMessages(ctx, lenMes) {
            let lengthMessages = ctx.state.lengthMessages
            if(ctx.state.lengthMessages < lenMes) {
                this._io[namespace].emit('GET_MESSAGES', {skip: lengthMessages, limit: lenMes - lengthMessages,  where: 1})
                this.commit(namespace + '/setLengthMessages', lenMes)
                this.dispatch(namespace + '/afterEmitGetNewMessages')
            } 
        },
        emitGetMessages(ctx, {skip,limit}) {
                this.commit(namespace+'/toggleWaitingMessages')
                this._io[namespace].emit('GET_MESSAGES', {skip, limit, where: -1})
        },
    },
    modules: {
        baseSocket,
    }
})