import Io   from 'socket.io-client'
const protocol = window.location.protocol
const ioChat   = Io(protocol + '//' + window.location.hostname + '/chat')
const ioConversation = Io(protocol + '//' + window.location.hostname + '/conversation', {
	autoConnect: false,
})
const ioNotification = Io(protocol + '//' + window.location.hostname + '/notification')
export default store => {
    store._io = {
        chat: ioChat,
        conversation: ioConversation,
        notification: ioNotification,
    }
}