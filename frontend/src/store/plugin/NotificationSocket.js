export default namespace => {	
    return function(store) {
        store._io[namespace].on('NEW_MESSAGE', (data) => {
            const urlParams = new URLSearchParams(window.location.search)
            const room = urlParams.get('room')
            if(data.data !== '' && data.room_id !== room) {
                store.commit('alert/addAlertMessage', {
                    title: data.username,
                    message: data.data.text,
                })
            }
            store.commit('nav/getCounter', 'conversation')
            store.dispatch('conversation/getRooms', true)
        })
    }
}