export default namespaces => {
    return function(store) {
        for(let nsp of namespaces) {
            store._io[nsp].on('connect', ()=> {
                console.log('socket '+nsp+' connected')
                store.commit(nsp + '/setConnected', true)
            })
            store._io[nsp].on('disconnect', () => {
                console.log('socket '+nsp+' disconnected')
                store.commit(nsp + '/setConnected', false)
            })
        }
    }
}