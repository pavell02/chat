export default namespace => {	
    return function(store) {
        store._io[namespace].on('SET_INTELOCUTOR_NAME', (data) => {
            store.commit(namespace + '/setIntelocutorName', data)
        })
    }
}