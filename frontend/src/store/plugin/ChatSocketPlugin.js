export default namespace => {	
    return function(store) {
        store._io[namespace].on('SET_STATE_CHAT', (data) => {
            store.commit(namespace + '/setStateChat', data)
        })
        store._io[namespace].on('COUNT_USERS', (count) => {
            store.commit(namespace + '/setCountUsers', count)
        })
        store._io[namespace].on('SET_ADD_FRIEND_ID', (id) => {
            store.commit(namespace + '/setAddFriendID', id)
        })
        store._io[namespace].on('SET_SEARCH_CHECKINGS', ({checked, max}) => {
            store.commit(namespace + '/setCheckedAmountSearch', checked)
            store.commit(namespace + '/setCheckedMaxSearch', max)
        })
        store._io[namespace].on('DO_NOT_DISTURB', () => {
            store.commit('alert/addAlertMessage', {
                message: 'Упс... Комната уже занята, выберите другую или обновите поиск.',
            })
        })
    }
}