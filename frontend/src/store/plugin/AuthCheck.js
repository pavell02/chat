import Ajax from '../../common/libs/ajax.js'
export default  function(store) {
	let checkAuth = () => {
			Ajax('/authenticated')
			.then(res => res.json())
			.then(({auth}) => {
				store.commit('auth/setAuth', auth)
			})
		}
		checkAuth()
	setInterval(checkAuth, 5000)
}