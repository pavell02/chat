export default namespace => {	
		return function(store) {
			store._io[namespace].on('PUSH_MESSAGES', ({messages, where}) =>{
				if(Array.isArray(messages)) {
					if(where === 1) {
						for(let i = 0; i < messages.length; i++) {
							store.commit(namespace + '/pushMessage', {message: messages[i], where})						
						}	
					}
					else if(where === -1) {
						for(let i = messages.length - 1; i >= 0; i--) {
							store.commit(namespace + '/pushMessage', {message: messages[i], where})						
						}	
					}
					store.commit(namespace + '/toggleWaitingMessages')	
				}
			})
			store._io[namespace].on('MESSAGES_ARE', (lenMes)  => {
				store.dispatch(namespace + '/emitGetNewMessages', lenMes)
			})
			store._io[namespace].on('SET_MESSAGES', ({messages, lenMes}) => {
				store.commit(namespace + '/setMessages', messages)
				store.commit(namespace + '/setLengthMessages', lenMes)
			})
			store._io[namespace].on('MARKED_AS_READ', ids => {
				store.commit(namespace + '/setMessagesAsRead', ids)
				store.dispatch(namespace + '/afterMarkedAsRead')
			})
		}
}