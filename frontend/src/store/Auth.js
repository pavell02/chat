export default () => ({
    namespaced: true,
	state: () => ({
		auth: false,
		view: false,
	}),
	mutations: {
		toggleAuthView(state) {
			if(!state.auth) {
				state.view = !state.view
			}
		},
		setAuth(state, auth) {
			if(state.view && auth) {
				this.commit('auth/toggleAuthView')
			}
			state.auth = auth
		}
	},
})