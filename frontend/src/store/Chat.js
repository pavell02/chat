import Vue from 'vue'
import ioStore from './ChatSocket'

const io = ioStore('chat')
//state chat
export const STATE_DATA_USER = 0
export const STATE_SEARCHING = 1
export const STATE_CHATING = 2
export const STATE_END_CHATING = 3
//topics of chat
export const TOPIC_OF_CHAT_TALK = 'talk'
export const TOPIC_OF_CHAT_FLIRT = 'flirt'
export const TOPIC_OF_CHAT_FAST_SEARCH = 'fast_search'
export const TAB_DIALOG = 'dialog'
export const TAB_ROOMS = 'rooms'

export default {
  namespaced: true,
  modules: {
	io,
  },
  state: () => ({
	tabs: [
		{
			k: TAB_ROOMS,
			v: 'Комнаты',
			disabled: false,
		},
		{
			k: TAB_DIALOG,
			v: 'Трансляции',
			disabled: true,
		},
	],
	activeTab: TAB_ROOMS,//localStorage.activeTab || TAB_DIALOG,
	tagsRoom: localStorage.tagsRoom ? JSON.parse(localStorage.tagsRoom) : [],
	tagsSearch: localStorage.tagsSearch ? JSON.parse(localStorage.tagsSearch) : [],
	checkedAmountSearch: 0,
	checkedMaxSearch: 0,
	recaptchaSearchView: false,
	addFriendID: localStorage.addFriendID || '',
	checkBeforeSearch: true,
	imageShow: true,
	error: {},
	stateChat: parseInt(localStorage.stateChat) || STATE_DATA_USER,
	countUsers: 0,
	newDescription: localStorage.newDescription || '',
	chaterAge: parseInt(localStorage.chaterAge, 10) || '',
	neighborAge: localStorage.neighborAge ? JSON.parse(localStorage.neighborAge) : ['', ''],
	chaterSex: localStorage.chaterSex ? localStorage.chaterSex : '',
	neighborSex: localStorage.neighborSex ? JSON.parse(localStorage.neighborSex) : ['', ''],
	topicOfChat: TOPIC_OF_CHAT_FAST_SEARCH,//localStorage.topicOfChat || TOPIC_OF_CHAT_TALK,
	//lastAddedMessagesLength: 0,
	statusSmiles: false,
	agreement: localStorage.agreement ? JSON.parse(localStorage.agreement) : false,
	gameState: false,
	socketMessages: [],
  }),
//   getters: {
// 	getMessages: state => {
// 		let messages = state.messages.map(el => {
// 			let time = new Date(el.time)
// 			if(time != 'Invalid Date') {
// 				let hours = time.getHours() < 10 ? `0${time.getHours()}` : time.getHours()
// 				let mins = time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes()	
// 				el.time = `${hours}:${mins}`
// 			}
// 			return el
// 		})
// 		return messages
// 	} 
//   },
  getters: {
	checkBeforeSend(state) {
		return (state.stateChat != STATE_CHATING) 
	},
  },
  mutations: {
	setActiveTab(state, v) {
		localStorage.activeTab = v
		Vue.set(state, 'activeTab', v)
	},
	setNewDecsription (state, val) {
		localStorage.newDescription = val
		Vue.set(state, 'newDescription', val)
	},
	setCheckedAmountSearch(state, checked) {
		Vue.set(state, 'checkedAmountSearch', checked)
	},
	setCheckedMaxSearch(state, max) {
		Vue.set(state, 'checkedMaxSearch', max)
	},
	setAddFriendID(state, id) {
		localStorage.addFriendID = id
		Vue.set(state, 'addFriendID', id)
	},
	updateChaterAge(state, age) {
		age = parseInt(age, 10)
		if (!isNaN(age)) {
			state.chaterAge = age
			localStorage.chaterAge = age
		}
		else {
			state.chaterAge = ''
			localStorage.chaterAge = ''
		}
	},
	setNeighborAge(state, {age,i}) {
		age = parseInt(age, 10)
		if (!isNaN(age)) {
			Vue.set(state.neighborAge, i, age)
			localStorage.neighborAge = JSON.stringify(state.neighborAge)
		}
		else {
			Vue.set(state.neighborAge, i, '')
			localStorage.neighborAge = JSON.stringify(state.neighborAge)
		}
	},
	setChaterSex(state, sex) {
		state.chaterSex = sex
		localStorage.chaterSex = sex
	},	
	setNeighborSex(state, i) {
		let neighborSex
		switch(i) {
			case 0 :
				neighborSex = state.neighborSex[i] ? '' : 'male'
				Vue.set(state.neighborSex, i, neighborSex)
			break
			case 1 :
				neighborSex = state.neighborSex[i] ? '' : 'female'
				Vue.set(state.neighborSex, i, neighborSex)
			break
		}
		localStorage.neighborSex = JSON.stringify(state.neighborSex)
	},
	setTopicOfChat(state, topic) {
		state.topicOfChat = topic
		localStorage.topicOfChat = topic
	},
	setCountUsers(state, count) {
		state.countUsers = count
	},
	toggleAgreement(state) {
		state.agreement = !state.agreement
		localStorage.agreement = state.agreement
	},
	toggleStatusSmiles(state) {
		state.statusSmiles = !state.statusSmiles
	},
	toggleGameState(state) {
		state.gameState = !state.gameState
	},
	emitSearch(state, {room, _id}) {
		let obj = !room ? {//state.activeTab == TAB_DIALOG ? {
			room: false,
			chaterAge: state.chaterAge,
			chaterSex: state.chaterSex,
			neighborAgeFrom: state.neighborAge[0],
			neighborAgeTo: state.neighborAge[1],
			neighborSex: state.neighborSex.filter(el => el),
			topicOfChat: state.topicOfChat
		}
		:
		{
			_id,
			room: true,
			tags: state.tagsRoom,
			description: state.newDescription,
		}
		this.commit('chat/setMessages', [])
		let checker = state.agreement && (state.activeTab == TAB_DIALOG ? (state.topicOfChat == TOPIC_OF_CHAT_FAST_SEARCH || state.checkBeforeSearch) :state.checkBeforeSearch) 
		if(checker) {
			state.error = {}
			this.commit('chat/setAddFriendID', '')
			if(state.checkedAmountSearch !== state.checkedMaxSearch) {
				this.commit('chat/toggleRecaptchaSearchView')
				setTimeout(() => grecaptcha.render( document.getElementById('recaptcha-search'), {
					'sitekey' : '6LfECZEaAAAAAKC2-zEO1KSDisvWsf_M-ZXuNdip',
					'hl' : 'ru',
					'callback' : function(token) {
						this.commit('chat/toggleRecaptchaSearchView')
						this._io.chat.emit('SEARCH', {...obj, token})
					}.bind(this),
				}), 0)
			}
			else {
				this._io.chat.emit('SEARCH', obj)
			}
			this.commit('chat/setLengthMessages', 0)
		}
	},
	toggleRecaptchaSearchView(state) {
		Vue.set(state, 'recaptchaSearchView', !state.recaptchaSearchView )
	},
	emitStopChat() {
		this._io.chat.emit('STOP_CHAT')
	},
	setStateChat(state, stateChat) {
		if(stateChat === STATE_CHATING) {
			MIDIjs.play('/static/media/tutum.mid')
		}
		localStorage.stateChat = stateChat
		state.stateChat = stateChat
	},
	setImageShow(state) {
		state.imageShow  =  !state.imageShow
	},
	setErrorBeforeSearch(state, room) {
		//tab
		let activeTab = state.activeTab
		//chat tab
		let chaterAge = state.chaterAge
		let chaterSex = state.chaterSex
		let neighborAgeFrom = state.neighborAge[0]
		let neighborAgeTo   = state.neighborAge[1]
		let neighborSex     = state.neighborSex
		let topicOfChat		= state.topicOfChat
		//room tab
		let tagsRoom = state.tagsRoom
		//newDescription = state.newDescription
		//checker
		let check = true
		let checkAge = (age) => {
			if(age <= 99 && age >= (topicOfChat == TOPIC_OF_CHAT_TALK || topicOfChat == TOPIC_OF_CHAT_FAST_SEARCH ? 14 : 18) && typeof age == 'number') {
				return true
			}
			else {
				return false
			}
		}
		state.error = {}
		if(!state.agreement) {
			Vue.set(state.error, 'agreement', true)
		} 
		else {
			if(activeTab == TAB_DIALOG ){
				if(!checkAge(chaterAge)) {
					Vue.set(state.error, 'chater_age', true)
					check = false
				}
				if(chaterSex == '') {
					Vue.set(state.error, 'chater_sex', true)
					check = false
				}
				if(!checkAge(neighborAgeFrom)) {
					Vue.set(state.error, 'neighbor_age_from', true)
					check = false
				}
				if(!checkAge(neighborAgeTo)) {
					Vue.set(state.error, 'neighbor_age_to', true)
					check = false	
				}
				if(neighborAgeTo < neighborAgeFrom) {
					Vue.set(state.error, 'neighbor_age_to', true)
					Vue.set(state.error, 'neighbor_age_from', true)
					check = false	
				}
				if(neighborSex[0] != 'male' && neighborSex[1] != 'female') {
					Vue.set(state.error, 'neighbor_sex', true)
					check = false
				}
			}
			else if(activeTab == TAB_ROOMS && room) {
				if(!tagsRoom.length) {
					Vue.set(state.error, 'tags_room', true)
					check = false
				}
			}
		}
		state.checkBeforeSearch = check
	}
  },
}
