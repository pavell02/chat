//import Io   from 'socket.io-client'
import Vue from 'vue'
import Ajax from '../common/libs/ajax'
import {PAGE_LIMIT} from '../views/friends/script'
import ioStore from './ChatSocket'
const io = ioStore('conversation')

const urlParams = new URLSearchParams(window.location.search)
const room = urlParams.get('room')

export default {
    namespaced: true,
    modules: {
        io,
    },
	state: () => ({
        rooms: [],
        intelocutorName: 'username',
        loading: false,
        roomsPage: 0,
        roomsStop: false,
        selectedRoomId: room || '',
        username: '',
	}),
	mutations: {
        setIntelocutorName(state, name) {
            state.intelocutorName = name
        },
        toggleLoading(state) {
            Vue.set(state, 'loading', !state.loading)
        },
        markedRoomAsRead(state) {
            for(let key in state.rooms) {
                if(state.rooms[key]._id == state.selectedRoomId) {
                    let obj = Object.assign(state.rooms[key].last_message)
                    obj.read = true
                    Vue.set(state.rooms[key], 'last_message', obj)
                    Vue.set(state.rooms[key], 'unread_messages', 0)
                }
            }
        },
        addRooms(state, {rooms, update}) {
			if(update) {
                Vue.set(state, 'rooms', rooms)
                return
            }
            if(rooms.length === PAGE_LIMIT+1) {
				//set page
				rooms.pop()
			}
			else {
				//set stop loading
				Vue.set(state, 'roomsStop', true)
			}
            Vue.set(state, 'roomsPage', state['roomsPage'] + 1)
			//set rooms to page
            state.rooms.push(...rooms)
            
			this.commit('toggleLoading')
        },
        setRoomId(state, id) {
            if(state.selectedRoomId != id) {
                this.dispatch('conversation/socketDisconnect')
                Vue.set(state, 'selectedRoomId', id)
                this.dispatch('conversation/socketConnect', id)
            }
        },
        setUsername(state, name) {
            Vue.set(state, 'username', name)
            Vue.set(state, 'rooms', [])
            Vue.set(state, 'roomsPage', 0)
            Vue.set(state, 'roomsStop', false)
            this.dispatch('conversation/getRooms')
        }
    },
    actions: {
        socketConnect(ctx, room_id) {
            this._io.conversation.query = {
                room_id,
            }
            this._io.conversation.connect()
        },
        socketDisconnect() {
            this._io.conversation.close()
        },
        async getRooms({state, commit}, update = false) {
            if(state.roomsStop && !update) return
            //set path
            let path = '/get-rooms'
            let username = state.username
            let roomsPage = state.roomsPage
			commit('toggleLoading')
            //set options
            let opts = {
                data: {
                    username,
                    limit: update ? PAGE_LIMIT * roomsPage : PAGE_LIMIT,
                    page: update ? 0 : roomsPage
                }
            }
			//send
			Ajax(path, opts)
			.then(res => res.json())
			.then((rooms)=>commit('addRooms', {rooms, update}))
			.catch(
				function(err) {
					console.error(err)
					commit('toggleLoading')
				}
			)
        },
        afterEmitGetNewMessages(ctx) {
            ctx.dispatch('getRooms', true)
        },
        afterMarkedAsRead({commit}) {
            this.commit('nav/getCounter', 'conversation')
            commit('markedRoomAsRead')
        }
    }
}