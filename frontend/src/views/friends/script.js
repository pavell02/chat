import Ajax from '../../common/libs/ajax.js'
import Users from '../../components/users/Users.vue'
import {mapMutations, mapState} from 'vuex'
import Vue from 'vue'

export const PAGE_LIMIT = 25
export const STATUS_FRIENDS = {
	ADD_FRIEND: 0,		//add to freinds
    REQUESTED: 1,    //'requested'
}
export 	function addFriend(id) {
	let path = '/add-friend'
	let opts = {
		method: 'POST',
		data: {
			recipient: id,
		},
	}
	Ajax(path, opts)
	.then(res => res.json())
	.then(function({message}) {
		if(message) {
			this.addAlertMessage({message})					
		}
	}.bind(this))
	.catch(
		function(err) {
			console.error(err)
		}
	)
}

export default {
	components: {
		Users,
	},
	data() {
		return {
			friends: [],
			friendsTitleTo: 'Написать сообщение',
			loading: false,
			friendsPage: 0,
			friendsStop: false,
			users: [],
			usersTitleTo: 'Добавить в друзья',
			usersPage: 0,
			usersStop: false,
			cancelRequestTitle: 'Отклонить заявку',
			headerFilterStatus: this.$route.query.filter || 'friends',
			enterUsername: this.$route.query.search || '',
			maxUsername: 20,
		}
	},
	methods: {
		...mapMutations({
			addAlertMessage: 'alert/addAlertMessage',
			getCounter: 'nav/getCounter',
			setRoomId: 'conversation/setRoomId',
		}),
		async findUsers() {
			let headerFilterStatus = this.headerFilterStatus
			let who = headerFilterStatus === 'GLOBAL' ? 'users' : 'friends'
			let username = this.enterUsername
			//cancel if
			if(
				this[who + 'Stop'] ||
				headerFilterStatus === 'GLOBAL' && username === '' ? true : false || 
				this.loading
			) return
			
			let usersPage = this.usersPage
			let friendsPage = this.friendsPage

			//set status for friends
			let status = {}
			if(who == 'friends') {
				status.status = STATUS_FRIENDS[headerFilterStatus]
			}

			//set options
			let opts = {
				data: {
					username,
					limit: PAGE_LIMIT,
					page: (headerFilterStatus === 'GLOBAL' ? usersPage : friendsPage) || 0,
					...status
				}
			}

			//set path
			let path = headerFilterStatus === 'GLOBAL'? '/find-user' : '/get-friends'
			this.toggleLoading()

			//send
			Ajax(path, opts)
			.then(res => res.json())
			.then(this.afterFindFiends)
			.catch(
				function(err) {
					console.error(err)
					this.toggleLoading()
				}
				.bind(this)
			)
		},
		afterFindFiends(data){
			let headerFilterStatus = this.headerFilterStatus
			let who = headerFilterStatus === 'GLOBAL' ? 'users' : 'friends'
			let wp = who + 'Page'
			
			if(data.length === PAGE_LIMIT+1) {
				//set page
				data.pop()
				Vue.set(this, wp, this[wp] + 1)
			}
			else {
				//set stop loading
				Vue.set(this, who + 'Stop', true)
			}
			//set data to page
			this[who].push(...data)
			this.toggleLoading()
		},
		addFriend,
		acceptRequest(id) {
			let path = '/accept-friend'
			let opts = {
				method: 'POST',
				data: {
					requester: id,
				},
			}
			Ajax(path, opts)
			.then(res => res.json())
			.then(function() {
				this.setDefault('friends')
				this.findUsers()
				this.getCounter(['friend'])
			}.bind(this))
			.catch(
				function(err) {
					console.error(err)
				}
			)
		},
		rejecteRequest(id) {
			let path = '/reject-friend'
			let opts = {
				method: 'POST',
				data: {
					requester: id,
				},
			}
			Ajax(path, opts)
			.then(res => res.json())
			.then(function() {
				this.setDefault('friends')
				this.findUsers()
				this.getCounter(['friend'])
			}.bind(this))
			.catch(
				function(err) {
					console.error(err)
				}
			)
		},
		openConversation(id) {
			for(let fr of this.friends) {
				if(fr._id === id) {
					this.setRoomId(fr.room_id)
					this.$router.push({ 
						path: 'conversation',
						query: { 
							room: fr.room_id,
						}
					})
					return
				}
			}	
		},
		setFilterStatus(status) {
			if(this.headerFilterStatus == status) return 

			Vue.set(this, 'headerFilterStatus', status)

			let search = this.enterUsername
			this.$router.push({ 
				path: 'friends',
				query: { 
					filter: status,
					search, 
				}
			})
			this.setDefault('friends')
			this.setDefault('users')
			this.findUsers()
		},
		setDefault(who) {
			Vue.set(this, who+'Page', 0)
			Vue.set(this, who+'Stop', false)
			Vue.set(this, who, [])
		},
		toggleLoading() {
			Vue.set(this,'loading', !this.loading)
		},
		actionTo(state) {
			
				// switch(state) {
				// 	case 'users':
				// 		return [
				// 			{
				// 				titleTo: this.usersTitleTo,
				// 				callbackTo: this.addFriend,
				// 			}
				// 		]
				// 	break
				// 	case 'friends':
				// 		return [
				// 			{
				// 				titleTo: this.friendsTitleTo,
				// 				callbackTo: ()=> true,
				// 			}
				// 		]
				// 	break
				// 	case 'pending':
				// 		return [
				// 			{
				// 				titleTo: this.usersTitleTo,
				// 				callbackTo: this.acceptRequest,
				// 			},
				// 			{
				// 				titleTo: this.cancelRequestTitle,
				// 				callbackTo: this.rejecteRequest,
				// 			}	
				// 		]
				// 	break
				// }
				if(state == 'users'){
					return [
						{
							titleTo: this.usersTitleTo,
							callbackTo: this.addFriend,
						}
					]
				}
				if(state == 'friends') {
					return [
						{
							titleTo: this.friendsTitleTo,
							callbackTo: this.openConversation,
						}
					]
				}
				if(state == 'pending'){
					return [
						{
							titleTo: this.usersTitleTo,
							callbackTo: this.acceptRequest,
						},
						{
							titleTo: this.cancelRequestTitle,
							callbackTo: this.rejecteRequest,
						}	
					]
				}
			
		}
	},
	created(){
		this.findUsers()
	},
	computed: {
		username: {
			get(){
				return this.enterUsername
			},
			set(data) {
				Vue.set(this, 'enterUsername', data)
				let filter = this.headerFilterStatus
				this.$router.push({ 
					path: 'friends',
					query: { 
						filter,
						search: data, 
					}
				})
				this.setDefault('friends')
				this.setDefault('users')
				this.findUsers()
			}
		},
		...mapState('nav', ['count']),
	}
}