import Form from '../../common/modules/form/Form.vue'
import {mapState, mapMutations} from 'vuex'
import Ajax from '../../common/libs/ajax.js'
import {checkStruct, structToFormData} from '../../common/libs/struct.js'

export default {
	components: {
		Form,
	},
	data() {
		return {
			restoreStruct:  [
				{
					caption: 'Введите новый пароль',
					placeholder: '********',
					name: 'password',
					value: '',
					min: 6,
					max: 30,
					type: 'password',
					same: 'password',
					empty: false,
				},
				{
					caption: 'Повторите пароль',
					placeholder: '********',
					value: '',
					type:'password',
					same: 'password',
					empty: false,
				},
			],
		}
	},
	computed: {
		...mapState({
		}),
		restore: function(){ return this.restoreStruct},
	},
	methods:{ 
		...mapMutations({
			toggleAuthView: 'auth/toggleAuthView',
			addAlertMessage: 'alert/addAlertMessage',
		}),
		onRestore() {
			if(this.checkStruct(this.restore)) {
				Ajax('/restore-password',
				{
					method: 'POST',
					data: {
						...structToFormData(this.restore),
						salt: this.$route.params.salt,
						username: this.$route.params.username,
					},
				})
				.then(res => res.json())
				.then(({message})=> {
					this.$router.push('/')
					this.toggleAuthView()
					this.addAlertMessage({message})
				})
			}
		},
		checkStruct,
	}
}