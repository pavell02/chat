import {positionScroll, returnHtml} from '../../common/libs/html'
import Messages from '../../components/messages/Messages.vue'
import Keyboard from '../../components/keyboard/Keyboard.vue'
import Smiles from '../../components/smiles/Smiles.vue'
import {mapActions, mapState, mapGetters, mapMutations} from 'vuex'
import Vue from 'vue'

export default {
    components: {
        Messages,
        Keyboard,
        Smiles,
    },
    data() {
        return {
            maxUsername: 20,
            imageShow: true,
            conView: false,
        }
    },
    methods: {
        ...mapMutations('conversation', [
            'setMessages',
            'setUsername',
            'setRoomId'
        ]),
        ...mapActions('conversation',[
            'socketConnect',
            'socketDisconnect',
			'emitGetMessages',
            'getRooms',
        ]),
        scrollTo(e) {
            let pos = positionScroll(e)
            if(pos == 'bottom') {
                this.getRooms()
            }
        },
        preSetRoomId(id) {
            this.$router.push({ 
                path: 'conversation',
                query: { 
                    room: id,
                }
            })
            this.setRoomId(id)
            this.setConView(true)
        },
        returnHtml,
        ...mapActions('conversation', [
            'emitMessagesAreRead',
            'emitMessage',
        ]),
        setConView(val) {
            if(!val) {
                this.$router.push({ 
                    path: 'conversation'
                })
                this.setRoomId()
            }
            Vue.set(this, 'conView', val)
        }
    },
    created() {
        this.getRooms()
        if(this.selectedRoomId) {
            this.socketConnect(this.selectedRoomId)
            this.setConView(true)
        }
    },
    computed: {
        ...mapState({
            mobileView: state => state.mobileView,
            connected: state => state.conversation.io.connected,
            lengthMessages: state => state.conversation.io.lengthMessages,
            waitingMessages: state => state.conversation.io.waitingMessages,
        }),
        ...mapState('conversation', [
            'intelocutorName',
            'rooms',
            'selectedRoomId',
            'username',
        ]),
		...mapGetters('conversation', [
            'messagesGetter',
            'connected'
        ]),
        usernameComputed: {
            get() {
                return this.username
            },
            set(val) {
                this.setUsername(val)
                this.setConView(false)
            }
        },
        dataForSendFile() {
            let room_id = this.selectedRoomId
            return {
                room_id,
            }
        },
    }
}