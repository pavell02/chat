import DataChat from '../../components/data-chat/DataChat.vue'
import Searching from '../../components/searching/Searching.vue'
import Chating from '../../components/chating/Chating.vue'
import SearchCaptcha from '../../components/search-captcha/SearchCaptcha.vue'
import {mapState, mapMutations, mapActions, mapGetters} from 'vuex'
import {addFriend} from '../friends/script'
import {TAB_DIALOG} from '../../store/Chat'

export default {
	data() {
		return {
			TAB_DIALOG,
		}
	},
	components: {
		DataChat,
		Searching,
		Chating,
		SearchCaptcha,
	},
	computed: {
		...mapState('chat', [
			'stateChat',
			'imageShow',
			'addFriendID',
			'recaptchaSearchView',
			'activeTab',
			'countUsers',
		]),
		...mapState('auth', [
			'auth',
		]),
		...mapGetters('chat', [
			'connected',
		]),
	},
	methods:{ 
		preAddFriendID() {
			if(!this.auth) {
				this.addAlertMessage({message: 'Авторизируйтесь чтобы добавить пользователя в друзья'})
				return
			}
			if(this.addFriendID) {
				this.addFriend(this.addFriendID)
			}
			else {
				this.addAlertMessage({message: 'Пользователь не авторизирован'})
			}
		},
		...mapMutations('chat', [
			'emitStopChat',
			'emitSearch',
			'setImageShow',
		]),
		...mapActions('chat', [
			'emitReturnToData',
		]),
		addFriend,
		...mapMutations('alert', ['addAlertMessage']),
	}
}