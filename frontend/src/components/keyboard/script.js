import Smiles from '../smiles/Smiles.vue'
import replaceHtmlFromMessage, {returnHtml} from '../../common/libs/html.js'
import {sendFile} from '../../common/libs/file.js'
import {mapMutations} from 'vuex'

export default {
	components: {
		Smiles,
	},
	data() {
		return {
			statusSmiles: false,
			text: '',
		}
	},
	props: [
		'messagesAreRead',
		'send',
		'imageTo',
		'dataForSendFile',
	],
	methods: {
		...mapMutations('alert', [
			'addAlertMessage'
		]),
		toggleStatusSmiles() {
			this.statusSmiles = !this.statusSmiles
		},
		setText() {
			if(this.returnHtml(this.$refs.mes.innerHTML).length < 1000) {
				this.text	= this.returnHtml(this.$refs.mes.innerHTML)
			}
		},
		sendMessageByEnter(event) {
			if(event.ctrlKey || event.shiftKey) return
			if(event.which == 13) this.sendMessage()
		},
		detectPaste(e) {
			let mes = e.clipboardData.getData('text/html') ||  e.clipboardData.getData('text/plain')
			mes 	= replaceHtmlFromMessage(mes)
			mes 	= this.returnHtml(mes)
			document.execCommand('insertHTML', false, mes)
			e.preventDefault()
		},
		returnHtml,
		sendMessage() {
			let mes = this.$refs.mes
			let text = replaceHtmlFromMessage(mes.innerHTML)
				.replace(/<div><br><\/div>$/,'')
			if(/^\s+$/.test(text) || text === '') return
			text = text.trim()
			if(text.length > 1500) text = text.splice(0,1500)
			this._props.send(text)
			this.$refs.mes.innerHTML = ''
		},
		sendImage(event) {
			let file	= event.target.files[0]
			let imgs	= ["image/jpeg", "image/gif", "image/png"]

			if(!imgs.includes(file.type)) return
			this.addAlertMessage({message: "Отправка изображения..."})
			sendFile(file, this.imageTo, 'image', this.dataForSendFile)
			.then(res => res.json())
			.then(({message}) => this.addAlertMessage({message}))
			.catch(({message}) => this.addAlertMessage({message}))
		},
	}
}