import { VueTagEditor } from '../../common/modules/tag-editor/main' 
export default {
    props: [
        'createNewRoomShow',
        'error',
        'tagsRoom',
        'lengthTags',
        'maxlength',
        'newDescription',
        'maxlengthDescription',
        'agreement',
        'toggleAgreement',
        'setErrorBeforeSearch',
        'emitSearch',
        'setNewDecsription',
    ],
    components: {
        VueTagEditor,
    },
    computed: {
        VnewDescription: {
			get() {
				return this.newDescription
			},
			set(val) {
				this.setNewDecsription(val)
			}
		},
    }
}