import {  mapMutations, mapState } from 'vuex'
export default {
	computed: {
		...mapState('chat', ['checkedAmountSearch', 'checkedMaxSearch']),
	},
	methods: {
		...mapMutations('chat', ['toggleRecaptchaSearchView']),
	}
}