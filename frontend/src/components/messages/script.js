import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
import Vue from 'vue'
import {returnHtml} from '../../common/libs/html.js'
import Smiles from '../smiles/Smiles.vue'

Vue.use(Viewer)

new Viewer.setDefaults({title: false})

export default {
	components : {
		Smiles,
	},
	props: [
		'getMessages',
		'imageShow',
		'setMessages',
		'lengthMessages',
		'emitGetMessages',
		'waitingMessages',
	],
	data() {
		return {
			messagesPrepared: false,
			scrollPosition: 'bottom',
			lastAddedMessagesWhere: 1,
			lastAddedMessagesLength: 0,
		}
	},
	methods: {
		filterImages(image){
			let filter = JSON.parse(image.getAttribute('data-filter'))
			if(filter) return image
		},
		toBottom() {
			let  innerMess = this.$refs.innerMessages
			let bottom = innerMess.scrollHeight
			innerMess.scrollTop = bottom			
		},
		setMessagesPrepared(bool) {
			this.messagesPrepared = bool
		},
		setLastAddedMessagesLength(len) {
			this.lastAddedMessagesLength = len
		},
		setLastAddedMessagesWhere(where) {
			this.lastAddedMessagesWhere = where
		},
		setMessageScroll() {
			let el = this.$refs.innerMessages
			let lastScrollPosition = this.scrollPosition
			
			//set current scroll position
			if(el.scrollTop  == 0 && el.scrollHeight > el.clientHeight && lastScrollPosition != 'top') {
				this.scrollPosition = 'top'
			}
			else if(
				((el.scrollHeight - el.clientHeight) == (Math.round(el.scrollTop) - 1))
				||
				((el.scrollHeight - el.clientHeight) == (Math.round(el.scrollTop) + 1))
				||
				((el.scrollHeight - el.clientHeight) == (Math.round(el.scrollTop)))
			) {
				this.scrollPosition = 'bottom'
			}
			else {
				this.scrollPosition = 'middle'
			}
			//get messages
			if(this.scrollPosition  === 'top' || el.scrollHeight == el.clientHeight) {
				let limit	= 25
				let skip	= this._props.lengthMessages - this.messages.length - limit
				while(skip  < 0 && limit !=  0) {
					skip	=  this._props.lengthMessages - this.messages.length - limit
					limit	+= skip
				}
				if(limit  > 0 && !this._props.waitingMessages)  {
					this.setLastAddedMessagesWhere(-1)
					this.setLastAddedMessagesLength(limit)
					this._props.emitGetMessages({skip, limit})
				}
			}
		},
		setCorrectPosition() {
			//set correct scroll position
			let el = this.$refs.innerMessages
			if(this.lastAddedMessagesWhere  == -1) {
				let height = 0
				for(let i = 0; i < this.lastAddedMessagesLength; i++) {
					height += el.childNodes[i].offsetHeight
				}
				el.scrollTop = height
				this.setLastAddedMessagesWhere(0)
				this.setLastAddedMessagesLength(0)
			}
		},
		preparingMessages() {
			if(this.messagesPrepared) {
				this.setMessagesPrepared(false)
				return
			}
			let getMessages = this._props.getMessages
			getMessages = JSON.parse(JSON.stringify(getMessages))
			let preparedMessages = getMessages.map(el => {
				let body_message = el.body_message
				if(body_message) {
					el.body_message = this.returnHtml(body_message)					
				}
				let time = new Date
				time.setTime(el.time)
				if(time != 'Invalid Date') {
					let hours = time.getHours() < 10 ? `0${time.getHours()}` : time.getHours()
					let mins = time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes()	
					el.time = `${hours}:${mins}`
				}
				return el
			})
			this.setMessagesPrepared(true)	
			this._props.setMessages(preparedMessages)	
		},
		afterFirstSetMessage() {
			if(this.scrollPosition == 'bottom') {
				this.toBottom()
			}
		},
		returnHtml,
	},
	computed: {
		messages(){
			return this._props.getMessages
		},
	},
	mounted() {
		this.preparingMessages()
	},
	updated() {
		this.preparingMessages()
		this.afterFirstSetMessage()
		this.setCorrectPosition()		
	}
}