import {mapMutations, mapActions, mapGetters} from 'vuex'
import Form from '../../common/modules/form/Form.vue'
import {checkStruct, structToFormData} from '../../common/libs/struct.js'
import Ajax from '../../common/libs/ajax.js'
import Vue from 'vue'
export default {
	components: {
		Form,
	},
	methods: {
		...mapMutations({
			toggleAuthView: 'auth/toggleAuthView',
			setAuth: 'auth/setAuth',
			addAlertMessage: 'alert/addAlertMessage',
		}),
		...mapActions('notification', [
			'socketConnect'
		]),
		setAuthState(state) {
			this.authState = state
		},
		onRegistration() {
			this.grecaptcha.render( document.getElementById('singup'), {
			  'sitekey' : '6Ld5jIgaAAAAAF3ppZikUBGa7YITVx9FrWAXAv1v',
			  'hl' : 'ru',
			  'callback' : function(token) {
				if(checkStruct(this.registrationStruct) && this.agreement) {
					Ajax('/registration', {
						data: {...structToFormData(this.registrationStruct), token},
						method: 'POST',
					})
					.then(res => res.json())
					.then(({message})=> {
						this.$router.push('/')
						this.toggleAuthView()
						this.addAlertMessage({message})
					})
				}
			  }.bind(this),
			})
		},
		onLogin() {
			this.grecaptcha.render( document.getElementById('login'), {
				'sitekey' : '6Ld5jIgaAAAAAF3ppZikUBGa7YITVx9FrWAXAv1v',
				'hl': 'ru',
				'callback' : function(token) {
					if(checkStruct(this.loginStruct)) {
						Ajax('/login', {
							data: {...structToFormData(this.loginStruct), token},
							method: 'POST',
						})
						.then((res) => res.json())
						.catch(err => {
							console.error(err)
							this.addAlertMessage({message: 'Введен не правильно логин или пароль'})
						})
						.then(({auth, message}) => {
							if(message) { 
								this.addAlertMessage({message})
							}
							else{
								this.$router.push('/')
								this.toggleAuthView()
								this.setAuth(auth)
								if(!this.connected) {
									this.socketConnect()
								}						
							}
						})
					}
				}.bind(this)
			})
		},
		onRestorePassword() {
			this.grecaptcha.render( document.getElementById('rp'), {
				'sitekey' : '6Ld5jIgaAAAAAF3ppZikUBGa7YITVx9FrWAXAv1v',
				'hl' : 'ru',
				'callback' : function (token) {	
					if(checkStruct(this.restorePasswordStruct)) {			
						Ajax('/confirm-restore-password', {
							data: {
								...structToFormData(this.restorePasswordStruct), 
								token
							},
						})
						.then(res => res.json())
						.then(({message})=> {
							this.$router.push('/')
							this.toggleAuthView()
							this.addAlertMessage({message})
						})
					}
				}.bind(this)
			})
		},
		checkStruct,
	},
	data() {
		let data = {
			grecaptcha,
			authState: 'auth',
			agreement: false,
			checkedEmail: false,
			checkedLogin: false,
			registrationStruct:  [
				{
					caption: 'Email',
					name: 'email',
					placeholder: 'example@ex.com',
					value: '',
					exists: function() {
						Vue.set(this, 'message', 'Проверка...')
						Ajax(`/exists/user/email/${this.value}`)
						.then(res => {
							Vue.set(this, 'message', 'Убедитесь, что почта верна. Дальше придет письмо на нее для подтверждения')
							return res.json()
						})
						.then(function({exists}){
							Vue.set(data, 'checkedEmail', !exists)
							if(exists) {
								Vue.set(this, 'error_message', 'Email уже существует')
							}
							else {
								Vue.set(this, 'error_message', undefined)
							}
						}.bind(this))
					},
					afterSettingValue() {
						this.exists()
					},
					empty: false,
				},
				{
					caption: 'Логин',
					name: 'username',
					placeholder: 'login',
					value: '',
					regularExpresion: {
						regexp: /^[A-Za-z0-9_-]+$/,
						signs: 'A-Z, a-z, 0-9, -, _'
					},
					exists: function() {
						Vue.set(this, 'message', 'Проверка...')
						Ajax(`/exists/user/username/${this.value}`)
						.then(res => {
							Vue.set(this, 'message', undefined)
							return res.json()
						})
						.then(function({exists}){
							Vue.set(data, 'checkedLogin', !exists)
							if(exists) {
								Vue.set(this, 'error_message', 'Логин уже занят')
							}
							else {
								Vue.set(this, 'error_message', undefined)
							}
						}.bind(this))
					},
					afterSettingValue() {
						this.exists()
					},
					min: 4,
					max: 20,
					empty: false,
				},
				{
					caption: 'Пароль',
					placeholder: '********',
					name: 'password',
					value: '',
					min: 6,
					max: 30,
					type: 'password',
					same: 'password',
					empty: false,
				},
				{
					caption: 'Повторите пароль',
					placeholder: '********',
					value: '',
					type:'password',
					same: 'password',
					empty: false,
				},
			],
			loginStruct: [
				{
					caption: 'Логин или Email',
					placeholder: 'login',
					name: 'username',
					value: '',
					empty: false,
				},
				{
					caption: 'Пароль',
					placeholder: '********',
					name: 'password',
					type: 'password',
					value: '',
					empty: false,
				},
			],
			restorePasswordStruct: [
				{
					caption: 'Email',
					placeholder: 'example@ex.com',
					name: 'email',
					value: '',
					empty: false,
				},
			],
		}
		return data
	},
	computed: {
		...mapGetters('notification', [
			'connecnted',
		]),
		registration: function(){ return this.registrationStruct},
		login: function(){ return this.loginStruct},
		restorePassword: function(){ return this.restorePasswordStruct},
	}
}
