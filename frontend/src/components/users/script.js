import {positionScroll} from '../../common/libs/html.js'
export default {
	data() {
		return {
		}
	},
	props: ['users', 'empty', 'scrollToCallback', 'actionsTo'],
	methods: {
		scrollTo(event, position, callback = () => true) {
			let pos = positionScroll(event)
			if(pos === position) {
				callback()
			}
		}
	},
}