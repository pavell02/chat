import { mapMutations, mapState, mapGetters} from 'vuex'

export default {
	components: {
	},
	methods: {
		...mapMutations('nav', ['toggleNavMobileView', 'getCounter']),
	},
	data() {
		return {
		}
	},
	computed: {
		...mapGetters('nav', ['getNav']),
		...mapState({
			auth: state => state.auth.auth,
		}),
	},
	created() {
		this.getCounter()
	}
}