import {mapMutations, mapState} from 'vuex'
export default {
	methods: {
		...mapMutations({
			toggleAuthView: 'auth/toggleAuthView',
		}),
		...mapMutations('nav', ['toggleNavMobileView']),
	},
	computed: {
		...mapState({
			auth: state => state.auth.auth,
			mobileView: state => state.mobileView,
		}),
	}
}