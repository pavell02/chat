export default {
	props: ['setText', 'toggleStatusSmiles'],
	methods: {
		addSmileToMessage(event) {
			let elem = event.toElement ? event.toElement : event.originalTarget
			let outerSmile = elem.outerHTML
			if(elem.tagName != 'IMG') return
			let refMes = this.$parent.$refs.mes
			refMes.innerHTML += outerSmile
			this._props.setText()
		}
	}
}