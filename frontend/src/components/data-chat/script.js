import { mapMutations, mapState } from 'vuex'
import { VueTagEditor } from '../../common/modules/tag-editor/main' 
import Paging from '../../common/modules/paging/Paging.vue'
import Ajax from '../../common/libs/ajax'
import Vue from 'vue'
import NewRoom from '../new-room/NewRoom.vue'

export default {
	components: {
		VueTagEditor,
		Paging,
		NewRoom,
	},
	data() { 
		return {
			tagsError: '',
			maxlength: 15,
			lengthTags: 8,
			count: 0,
			maxlengthDescription: 500,
			createNewRoomShow: false,
			loadingRooms: false,
			rooms: []
		}
	},
	watch: {
		tags(arr){
			if(arr.length > this.lengthTags) {
				arr.splice(this.lengthTags)
				this.tagsError = 'Максимальное число тегов - ' + this.lengthTags
			}
		},
		tagsRoom(arr) {
			localStorage.tagsRoom = JSON.stringify(arr)
		},
		tagsSearch(arr) {
			localStorage.tagsSearch = JSON.stringify(arr)
		}
	},
	computed:{
		...mapState({
			agreement: state => state.chat.agreement,
			chaterSex: state => state.chat.chaterSex,
			neighborSex: state => state.chat.neighborSex,
			error: 		state => state.chat.error,
			countUsers: state => state.chat.countUsers,
			topicOfChat: state => state.chat.topicOfChat,
			tagsRoom: state => state.chat.tagsRoom,
			tagsSearch: state => state.chat.tagsSearch,
			tabs: state => state.chat.tabs,
			activeTab: state => state.chat.activeTab,
			newDescription: state => state.chat.newDescription,
		}),
		chaterAge: {
			get() {
				return this.$store.state.chat.chaterAge
			},
			set(val) {
				this.updateChaterAge(val)
			}
		},
		neighborAgeFrom: {
			get() {
				return this.$store.state.chat.neighborAge[0]
			},
			set(age) {
				this.setNeighborAge({age, i:0})
			}
		},
		neighborAgeTo: {
			get() {
				return this.$store.state.chat.neighborAge[1]
			},
			set(age) {
				this.setNeighborAge({age, i:1})
			}
		},
	},
	methods: {
		...mapMutations('alert', [
			'addAlertMessage'
		]),
		...mapMutations('chat', [
			'toggleAgreement',
			'setChaterSex',
			'setNeighborSex',
			'emitSearch',
			'setErrorBeforeSearch',
			'setTopicOfChat',
			'updateChaterAge',
			'setNeighborAge',
			'setNewDecsription',
			'setActiveTab',			
		]),
		toggleCreateNewRoomShow() {
			this.createNewRoomShow = !this.createNewRoomShow
		},
		setCountRooms(count) {
			Vue.set(this, 'count', count)
		},
		queryRooms(opt = {skip: 0, limit: 25}, returnRes = false) {
			Vue.set(this, 'loadingRooms', true)
			let data = this.tagsSearch.length ? {tags:JSON.stringify(this.tagsSearch),...opt} : {...opt}
			let res = Ajax('/chat-rooms', {data})
			.finally(() => {
				Vue.set(this, 'loadingRooms', false)
			})
			if(!returnRes) {
				res.then(res => res.json())
				.then((data) => {
					Vue.set(this, 'rooms', data.data)
					this.setCountRooms(data.count)
				})
			}
			else {
				return res
			}
		},
		updateSearch() {
			Vue.set(this, 'rooms', [])
			while(this.tagsSearch.length) {
				this.tagsSearch.pop()
			}
			this.queryRooms()
		},
		enterInRoom(_id) {
			if(!this.agreement) {
				if(!this.createNewRoomShow) {
					this.toggleCreateNewRoomShow()
				}
				Vue.set(this.error, 'agreement', true)
			}
			else {
				this.emitSearch({_id, room: true})
			}
		}
	},
}