import Messages from "../messages/Messages.vue"
import Keyboard from "../keyboard/Keyboard.vue"
import { mapMutations, mapState, mapGetters, mapActions} from 'vuex'
import NewRoom from '../new-room/NewRoom.vue'
import { TAB_DIALOG, TAB_ROOMS } from "../../store/Chat"
//import {returnHtml} from '../../common/libs/html.js'

export default {
	components: {
		Messages,
		Keyboard,
		NewRoom,
	},
	data() {
		return {
			createNewRoomShow: false,
			TAB_DIALOG,
			TAB_ROOMS,
			tagsError: '',
			maxlength: 15,
			lengthTags: 8,
			maxlengthDescription: 500,
		}
	},
	computed: {
		...mapState('chat', [
			'stateChat',
			'imageShow',
			'newDescription',
		]),
		...mapState({
			lengthMessages: state => state.chat.io.lengthMessages,
			waitingMessages: state => state.chat.io.waitingMessages,
			activeTab: state => state.chat.activeTab,
			tagsRoom: state => state.chat.tagsRoom,
			error: state => state.chat.error,
			agreement: state => state.chat.agreement,
        }),
		...mapGetters('chat', ['messagesGetter']),
	},
	methods: {
		...mapMutations('chat', [
			'toggleStatusSmiles',
			'emitSearch',
			'setMessages',
			'setNewDecsription',
			'setErrorBeforeSearch',
			'toggleAgreement',
		]),
		...mapActions('chat', [
			'emitMessage',
			'emitReturnToData',
			'emitMessagesAreRead',
			'emitGetMessages',
		]),
		filterImages(image){
			let filter = JSON.parse(image.getAttribute('data-filter'))
			if(filter) return image
		},
		toggleCreateNewRoomShow() {
			this.createNewRoomShow = !this.createNewRoomShow
		},
		// prepareMessagesForEndMessages(stateChat) {
		// 	if(stateChat == 3) {
		// 		setTimeout(this.setHeightInnerMessage, 0)
		// 		return true
		// 	}
		// 	return false
		// },
		// preparingMessages() {
		// 	if(this.$store.state.chat.messagesPrepared) return
		// 	let preparedMessages = []
		// 	let getMessages = this.$store.getters['chat/getMessages']
		// 	getMessages = JSON.parse(JSON.stringify(getMessages))
		// 	for(let i in getMessages){
		// 		let text = getMessages[i].text
		// 		if(text) {
		// 			getMessages[i].text = returnHtml(text)					
		// 		}
		// 		preparedMessages.push(getMessages[i])
		// 	}	
		// 	this.$store.commit('chat/setMessages', preparedMessages)
		// },
		sendMessage(text) {
			if(this.$store.state.chat.stateChat != 2) return
			this.emitMessage(text)
		},
	},
}