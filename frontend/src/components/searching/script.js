import Game from '../game/Game.vue'
import { mapMutations, mapState } from 'vuex'

export default {
	components: {
		Game,
	},
	computed: {
		...mapState({
			gameState: state => state.chat.gameState,
		}),
	},
	methods: {
		...mapMutations({
			toggleGameState: 'chat/toggleGameState',
		})
	}
}