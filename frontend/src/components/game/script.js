import Netwalk from 'netwalk-vue'
import { mapMutations } from 'vuex'
import SlidePuzzle from 'vue-slide-puzzle'
import Maze from 'vue-maze'
import Vue from 'vue'

export default {
	components: {
		Netwalk,
		SlidePuzzle,
		Maze,
	},
	data() {
		return {
			listGames: ['netwalk', 'maze', 'puzzle'],
			game: '',
			customStyle: {
				cellSizePx: 50,
				connectionWidthPercent: 20,
				connectionColor: '#e1e1e1',
				connectionColorConnected: '#6b8fd4',			    
				cellColor: 'transparent',			    
				emptyCellColor: 'transparent',			    
				providerColor: '#ff6200',			    
				consumerColor: '#ffdc73',
				consumerColorConnected: '#ffbf00',			    
				rotateAnimation: true,
				rotateAnimationDuration: '400ms',
				colorAnimation: true,
				colorAnimationDuration: '150ms'
			},
		}
	},
	methods: {
		...mapMutations({
			toggleGameState: 'chat/toggleGameState',
		}),
		setGame() {
			let key = Math.floor(Math.random() * (this.listGames.length))
			Vue.set(this, 'game', this.listGames[key])
		}
	},
	created() {
		this.setGame()
	}
}