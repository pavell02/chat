import { mapMutations, mapState} from 'vuex'
export const TIME_OUT = 15000
export default {
	computed: {
		log(){},
		...mapState({}),
		getAlerts() {
			let alerts = this.$store.getters['alert/getAlertMessages']
			if(alerts.length && alerts[0].timeout === undefined) {
				this.deleteFirstItem(0)
			}
			return alerts
		},
	},
	data() {
		return {
			timeouts: [],
		}
	},
	methods: {
		...mapMutations({
			deleteAlertMessage: 'alert/deleteAlertMessage',
			setAlertMessageTimeout: 'alert/setAlertMessageTimeout',
			unsetAlertMessageTimeout: 'alert/unsetAlertMessageTimeout',
		}),
		deleteFirstItem() {
				//set settimeout
				let timeout = setTimeout(
					function() {
						this.deleteAlertMessage(0)
					}.bind(this)
					, TIME_OUT)
				this.setAlertMessageTimeout(timeout)
		}
	}
}