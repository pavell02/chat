import Vue from 'vue'
import VueRouter from 'vue-router'
import Chating from '../views/chat/Chat.vue'
import RestorePassword from '../views/restore-password/RestorePassword.vue'
import Friends from '../views/friends/Friends.vue'
import Conversation from '../views/conversation/Conversation.vue'
import Rules from '../views/rules/Rules.vue'
import vueDocumentTitlePlugin from 'vue-document-title-plugin'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Chating',
    component: Chating,
    meta: { title: "Чат" }
  },
  {
    path: '/restore-password/:username/:salt',
    name: 'RestorePassword',
    component: RestorePassword,
  },
  {
    path: '/friends',
    name: 'Friends',
    component: Friends,
    meta: { title: "Друзья" }
  },
  {
    path: '/conversation/:id',
    name: 'Conversation',
    component: Conversation,
    meta: { title: "Сообщения" }
  },
  {
    path: '/conversation',
    name: 'Conversation',
    component: Conversation, 
    meta: { title: "Сообщения" }
  },
  {
    path: '/rules',
    name: 'Rules',
    component: Rules, 
    meta: { title: "Правила" }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
Vue.use(vueDocumentTitlePlugin, router)

export default router
