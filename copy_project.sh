cd ~/chat/frontend
sudo npm install
sudo npm run build
cd ~/chat/backend
sudo npm install
sudo cp -r ~/chat /var/www
sudo mkdir -p /var/www/chat/backend/static/tmp_upload
sudo mkdir -p /var/www/chat/backend/static/media/images
sudo ln -s /var/www/chat/apogovorim_nginx.conf /etc/nginx/sites-enabled/apogovorim_nginx.conf
sudo nginx -t
sudo systemctl restart nginx
sudo npm install pm2 -g
sudo pm2 delete all
sudo pm2 start /var/www/chat/backend/index.js --name apogovorim --log /var/log/apogovorim.log
sudo pm2 save
sudo pm2 startup