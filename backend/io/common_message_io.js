const Utils = require('./plugins/utils')
const Log   = require('../utils/log')

class IO {
    constructor(io, socket, model, room_id, subscriber, namespace) {
        this._io = io
        this._socket = socket
        this._model = model
        this._room_id = room_id
        this._subscriber = subscriber
        this._nsp = namespace
        this.set_listeners()
    }

    set_listeners() {
        let socket = this._socket
        socket.on('MESSAGE', this.on_message.bind(this))
        socket.on('MESSAGES_ARE_READ', this.on_messages_are_read.bind(this))
        socket.on('GET_MESSAGES', this.on_get_messages.bind(this))
        socket.on('disconnect', this.on_disconnect.bind(this))
		this._subscriber.on('message', this.subscribe_image.bind(this))
    }

    subscribe_image(channel, data) {
        switch(channel){
            case this._nsp + '_image_' + this._chater_id:
                this.on_messages_are()
            break
        }
    }

    subscribe() {
        this._subscriber.subscribe(this._nsp + '_image_' + this._chater_id)
    }
    on_connect() {}

    on_disconnect() {
        this.unsubscribe()
    }

    unsubscribe() {
        this._subscriber.unsubscribe()
    }
    
    first_load(messages, lenMes) { 
        let io = this._io
        let socket = this._socket
        if(messages === null) return
        for(let i = messages.length - 1; i >= 0; i--) {
            messages[i].i = lenMes - 1 - (messages.length - 1 - i)
            messages[i].yours = messages[i].whose.toString() == this._chater_id.toString()
            delete messages[i].whose
        }
        io.in(socket.id).emit('SET_MESSAGES', {messages, lenMes})
                
    }

    async on_message(data) {
        this.limiter('message', 5)
        let members = await Utils.save_message_to_db(data, this._model, this._room_id, this._chater_id)
    	await this.on_messages_are(members, data)
    }

    limiter(event, max) {
        let limiters = this.limiters = this.limiters || {}
        let limiter = limiters[event] =
        limiters[event]
        ||
        {
            time: Math.ceil(Date.now() / 1000),
            counter: 0,
            max
        }
        if(limiter.time !== Math.ceil(Date.now() / 1000)) {
            limiters[event] = undefined
        }
        else {
            ++limiter.counter === limiter.max && this._socket.disconnect() 
        }
    }

    async on_messages_are(members, data) {
        let room_id = this._room_id
        let model = this._model
        let lengthMes = await model.aggregate([{$match: {_id: room_id}}, {$project: {messages_length: {$size: '$messages'}}}])
        if(lengthMes.length) {
            this._io.in(room_id).emit('MESSAGES_ARE', lengthMes[0].messages_length)
            this.after_on_messages(members, data)
        }
    }

    after_on_messages() {
    }
    
    async on_messages_are_read(ids) {
        try{
            let model = this._model
            let room_id = this._room_id
            let whereQuery = {
                _id: room_id,
            }
            let updateQuery = {}
            for(let i in ids) {
                let query = 'messages.'+ids[i]+'.read'
                updateQuery[query] = true
            }
            let members = await model.findOneAndUpdate(
                whereQuery,
                updateQuery
            ).select('members')

            this.after_on_messages(members, '')
            
            this._io.in(room_id).emit('MARKED_AS_READ', ids)
        }
        catch(e)  {
            Log.error(e)
        }		
    }

    async on_get_messages({skip, limit, where}){
        try{	
            let model = this._model
            let socket = this._socket
            let room_id = this._room_id
                //get messages
                let room = await model.findOne({
                    _id: room_id,
                })
                .select('messages')
                .slice('messages', [skip, limit])
                room = room.toObject()
                //set for messages their indexes
                let messages 	= room.messages
                for(let i in messages) {
                    i = parseInt(i)
                    if(!Number.isInteger(i)) continue
                    messages[i].i = skip + i
                    messages[i].yours = messages[i].whose.toString() == this._chater_id.toString()
                    delete messages[i].whose
                }
                this._io.in(socket.id).emit('PUSH_MESSAGES', {messages, where})
        }
        catch(e) {
            Log.error(e)
        }			
    }
}

module.exports = IO