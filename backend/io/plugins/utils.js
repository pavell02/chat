const Path		= require('path')
const Escape	= require('escape-html')
const CrRand	= require('crypto-random-string')

module.exports = {
	async save_message_to_db(data, model, room_id, chater_id, path_to_file) {
		let type = data.type
			let obj  = {
				type,
				time: Date.now(),
				read: false,				
			}
			//prepare data
			switch(type) {
				case('text'):
					let {text} = data
					if(text == '' || /^\s+$/.test(text)) return
					text = Escape(text)
					obj.body_message = text				
				break
				case('image'):
					let fileName  = data.name 
					obj.body_message = path_to_file +fileName
				break
			}
			
			let message = {
				whose: chater_id,
				...obj
			}
	
			let path = message.path_to_image
	
			if(path !== undefined) {
				let name  = CrRand({length: 35, type: 'url-safe'})
				let fileName  = name + Path.extname(data.name)
				try{
					Fs.copyFileSync('./static' + path, './static/tmp_upload/' + fileName )
					message.path_to_image = '/tmp_upload/' + fileName
				}
				catch(err) {
					console.error(err)
					message.path_to_image = 	obj.path_to_image
				}
			}
			try{
				//create message
				let pushMessage =  {
							messages: message,
						}
				if(message.path_to_image) {
					pushMessage.images = message.path_to_image
				}
				
				//send message
				return await model.findOneAndUpdate(
					{
						_id: room_id,
						members: chater_id,
					},
					{
						$push: pushMessage
					}
				).select('members')
			}
			catch(err) {
				console.error(err)
				let unlinks = [
					obj.path_to_image,
					message.path_to_image
				]
				for(let i in unlinks) {
					Fs.unlink('./static'+unlinks[i])
				}
			}
	}
	// async saveMessageToDb(data, chaterId) {
	// 	let type = data.type
	// 		let obj  = {
	// 			type,
	// 			time: Date.now(),
	// 			read: false,				
	// 		}
	// 		//prepare data
	// 		switch(type) {
	// 			case('text'):
	// 				let {text} = data
	// 				if(text == '' || /^\s+$/.test(text)) return
	// 				text = Escape(text)
	// 				obj.text = text				
	// 			break
	// 			case('image'):
	// 				let fileName  = data.name 
	// 				obj.path_to_image = '/static/tmp_upload/'+fileName
	// 			break
	// 		}
	// 		//set neighbor id
	// 		let chater = await Chater.findOne(
	// 			{
	// 				chater_id: chaterId,
	// 				state_chat: 2,
	// 			}
	// 		)
	// 		.select('neighbor_id')
	// 		let neighborId = chater.neighbor_id
	// 		//check neighbor
	// 		if(!neighborId) return 
			
	// 		let chaterMessage = {
	// 			yours: true,
	// 			...obj
	// 		}

	// 		let path = chaterMessage.path_to_image

	// 		if(path !== undefined) {
	// 			let name  = CrRand({length: 35, type: 'url-safe'})
	// 			let fileName  = name + Path.extname(data.name)
	// 			try{
	// 				Fs.copyFileSync('./static' + path, './static/tmp_upload/' + fileName )
	// 				chaterMessage.path_to_image = '/tmp_upload/' + fileName
	// 			}
	// 			catch(err) {
	// 				console.error(err)
	// 				chaterMessage.path_to_image = 	obj.path_to_image
	// 			}
	// 		}
	// 		try{
	// 			await Acid(async (session) => {
	// 				//create message for chater
	// 				let pushChater =  {
	// 							messages: chaterMessage,
	// 						}
	// 				if(chaterMessage.path_to_image) {
	// 					pushChater.images = chaterMessage.path_to_image
	// 				}
	// 				//send message to chater
	// 				await Chater.updateOne(
	// 					{
	// 						chater_id: chaterId,
	// 						state_chat: 2,
	// 					},
	// 					{
	// 						$push: pushChater
	// 					},
	// 					{session}
	// 				)
	// 				//create message for neighbor
	// 				let pushNeighbor =  {
	// 							messages: obj,
	// 						}
	// 				if(obj.path_to_image) {
	// 					pushNeighbor.images = obj.path_to_image
	// 				}
	// 				//send message to neighbor
	// 				await Chater.updateOne(
	// 					{
	// 						chater_id: neighborId,
	// 						state_chat: 2,
	// 					},
	// 					{
	// 						$push: pushNeighbor
	// 					},
	// 					{session}
	// 				)
	// 			})
	// 		}
	// 		catch(err) {
	// 			console.error(err)
	// 			let unlinks = [
	// 				obj.path_to_image,
	// 				chaterMessage.path_to_image
	// 			]
	// 			for(let i in unlinks) {
	// 				Fs.unlink('./static'+unlinks[i])
	// 			}
	// 		}
	// }
}