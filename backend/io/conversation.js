const MessageIO        = require('./common_message_io')
const Room	= require('../db/models/room.js')
const mongoose  = require('mongoose')
const Redis		= require('redis')
const Log       = require('../utils/log')

const subscriber = Redis.createClient()

subscriber
.on('error', err => Log.error(err))
.on('connect', () => Log.info('redis conversation subscriber is connected'))


const publisher	= Redis.createClient()

publisher.on('error', err => Log.error(err))
.on('connect', () => Log.info('redis publisher message conversation connected'))

class ConversationIO extends MessageIO {
    constructor(io, socket, model, room_id, chater_id) {
        super(io, socket, model, room_id, subscriber, 'conversation')
        this._chater_id = chater_id
        this.on_connect()
    }

    async on_connect() { 
        let io = this._io
        let socket = this._socket
        let user_id = mongoose.Types.ObjectId(socket.request.user._id)
        let room_id = this._room_id
        let room = await this._model.aggregate([
            {
                $match: {
                    _id: room_id,
                    members: user_id,
                },
            },
            {
                $addFields: {
                    messages: {
                        $ifNull: ['$messages', []]
                    }
                }
            },
            {
                $project: {
                    members: {
                        $filter: {
                            input: '$members',
                            as: 'member',
                            cond: {$ne: ['$$member', user_id]}
                        }
                    },
                    messages: '$messages'
                }
            },
            {
                $lookup: {
					from: "users",
					localField: 'members',
					foreignField: "_id",
					as: "user"
                }
            },
            {
                $project: {
                    messages: {
                        $slice: ['$messages', -50], 
                    },
                    messages_length: {
                        $size: '$messages',
                    },
                    intelucutor_name: '$user.username'
                }
            }
        ])
        room = room[0]
        if(room) {		
            socket.join(this._room_id)
            this.subscribe()
            io.in(socket.id).emit('SET_INTELOCUTOR_NAME', room.intelucutor_name[0])
            this.first_load(room.messages, room.messages_length)
        }
    }

    after_on_messages({members, _id}, data) {
        let to = members[0].toString() == this._chater_id.toString() ? members[1].toString() : members[0].toString()
        let username = this._socket.request.user.username
        publisher.publish('notification_new_message_'+ to, JSON.stringify({
            username,
            data,
            room_id: _id,
        }))
    }

}

let onConnect = function(IO) {
	return  function(socket) {
        let room_id = socket.handshake.query.room_id
        if(room_id !== 'undefined') {
            new ConversationIO(IO, socket, Room.user_room, mongoose.Types.ObjectId(room_id), mongoose.Types.ObjectId(socket.request.user._id))
	    }
        else socket.disconnect()
    }
}
module.exports = function(socket) {
	socket.on('connection',onConnect(socket))
}