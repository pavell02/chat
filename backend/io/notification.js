const Redis		= require('redis')
const Log       = require('../utils/log')

const subscriber = Redis.createClient()

subscriber
        .on('error', err => Log.error(err))
        .on('connect', () => Log.info('redis notification subscriber is connected'))
        
class NotificationIO {
    constructor(io, socket) {
        this._nsp = 'notification'
        this._io = io
        this._socket = socket
        this._room_name = this._nsp + '_' + socket.request.user._id
        this._chater_id = socket.request.user._id
        socket.join(this._room_name)
        this.on_connect()
        this.set_listeners()
    }

    set_listeners() {
        let socket = this._socket
        socket.on('disconnect', this.on_disconnect.bind(this))
    }
    
    on_connect() {
        let new_message_channel = this._nsp + '_new_message_' + this._chater_id
        if(!subscriber.subscription_set.hasOwnProperty('subscribe_' + new_message_channel)){
            subscriber.subscribe(new_message_channel)
        }
        subscriber.on('message', function(channel, data) {
            data = JSON.parse(data)
            switch(channel) {
                case new_message_channel:
                    this._io.in(this._socket.id).emit("NEW_MESSAGE", data)
                break
            }
        }.bind(this))
    }

    on_disconnect() {
        subscriber.unsubscribe()
    }
}
let onConnect = function(IO) {
	return  function(socket) {
		//new ConversationIO(IO, socket, Room.user_room, mongoose.Types.ObjectId(socket.handshake.query.room_id), mongoose.Types.ObjectId(socket.request.user._id))
        new NotificationIO(IO, socket)
    }
}
module.exports = function(socket) {
	socket.on('connection',onConnect(socket))
}