const Chater	= require('../db/models/chater.js')
const Room	= require('../db/models/room.js')
const Acid		= require('mongoose-acid')
//const Utils		= require('./plugins/utils.js')
const Redis		= require('redis')
const MessageIO        = require('./common_message_io')
const Fs		= require('fs')
const mongoose  = require('mongoose')//.Types.ObjectId
const cookie 	= require('cookie')
//const chater = require('../db/models/chater.js')
const Log 		= require('../utils/log')
const Got		= require('got')
const ENV = require('dotenv').config({
	path: '/var/www/chat/backend/.env',
}).parsed

const publisher	= Redis.createClient()

publisher.on('error', err => Log.error(err))
.on('connect', () => Log.info('redis publisher connected'))

const subscriber = Redis.createClient()

subscriber
.on('error', err => Log.error(err))
.on('connect', () => Log.info('redis subscriber connected'))

const stateStopChat = {
						state_chat: 3,
						neighbor_id: '',
					}
const TIME_OUT_DESTROY = 150*1000

const CACHED_IDS = {}
const MAX_CHECKINGS_SEARCH = 3

var CHATER_ROOMS = []
var CLEANING_OF_JUNK_CHATERS = true

class ChatIO extends MessageIO {
    constructor(io, socket, model, chater_model) {

        super(io, socket, model, undefined, subscriber, 'chat')
        this._chater_model = chater_model
        this.on_connect()
    }
    set_listeners() {
        super.set_listeners()
        let socket = this._socket
        socket.on('SEARCH', this.on_search.bind(this))
		socket.on('SET_ROOM_ID', this.on_set_room_id.bind(this))
        socket.on('RETURN_TO_DATA', this.on_return_to_data.bind(this))
		socket.on('STOP_CHAT', this.on_stop_chat.bind(this))
		socket.on('disconnect', this.on_disconnect.bind(this))
		this._subscriber.on('message', function(channel, data) {
			this.subscribe_image(channel, data)
			// switch(channel){
			// 	case 'example chanel':
			//  //do smt		
			// 	break
			// }
		}.bind(this))
    }
    async on_connect() {
        let socket = this._socket
        let io = this._io
        let chater_model = this._chater_model
		let chater_id = cookie.parse(socket.request.headers.cookie).chater_id
		this._chater_id = chater_id
		Log.info('socket connected chater id: ', chater_id)
		socket.join(chater_id)
		this.subscribe()		
		io.in(chater_id).emit('SET_SEARCH_CHECKINGS', {
			checked: CACHED_IDS[chater_id] || 0,
			max: MAX_CHECKINGS_SEARCH,
		})
        try{
			let chater = await chater_model.aggregate([
				{
					$match: {
						chater_id,
					}
				},
				{
					$lookup: {
						from: 'chater_rooms',
						localField: 'room_id',
						foreignField: '_id',
						as: 'room',
					},
				},
				{
					$project:
					{
						room_id: '$room_id',
						room: { 
							$arrayElemAt: ['$room', 0]
						},
						state_chat: '$state_chat',
					}
				},
				{
					$project:
					{
						room_id: '$room_id',
						messages: {
							$slice: ['$room.messages', -50], 
						},
						state_chat: '$state_chat',
						messages_length: {
							$cond: {
								if: '$room',
								then: {
									$size: '$room.messages',
								},
								else: 0
							}
						}
					}
				}
			])
			
			chater = chater[0]

            if(!chater) {
                io.in(chater_id).emit('SET_STATE_CHAT', 0)
            }
            else {
                io.in(chater_id).emit('SET_STATE_CHAT', chater.state_chat)
                if([2, 3].includes(chater.state_chat)) {
					this._room_id = chater.room_id
					socket.join(this._room_id)
					this.first_load(chater.messages, chater.messages_length)}
			}
			this.count_users()
        }
        catch(err) {
            Log.error(err)
        }
    }
	async check_search(token) {
		//check bot
		let recaptcha_url = "https://www.google.com/recaptcha/api/siteverify?";
		recaptcha_url += "secret=" + ENV.RECAPTCHA_SECRET_KEY_SEARCH + "&"
		recaptcha_url += "response=" + token
		let io = this._io
		try {
			let response = await Got(recaptcha_url)
			if(JSON.parse(response.body).success) {
				Log.debug(typeof JSON.parse(response.body).success)
				let chater_id = this._chater_id
				CACHED_IDS[chater_id] = CACHED_IDS[chater_id] || 0
				++CACHED_IDS[chater_id]
				io.in(chater_id).emit('SET_SEARCH_CHECKINGS', {
					checked: CACHED_IDS[chater_id] || 0,
					max: MAX_CHECKINGS_SEARCH,
				})
				return false
			}
			else return true
		}
		catch (err) {
			Log.error(err)
		}
	}
    async on_search(data) {
        let {
            chaterAge,
            chaterSex,
            neighborAgeFrom,
            neighborAgeTo,
            neighborSex,
            topicOfChat,
			room,
			tags,
			description,
			token,
			_id,
        } = data,
        dataChater =  !room ?
		(topicOfChat !== 'fast_search' ?			
        {
			chater_age: chaterAge,
            chater_sex: chaterSex,
            neighbor_age_from: neighborAgeFrom,
            neighbor_age_to: neighborAgeTo,
            neighbor_sexes: neighborSex,
            topic_of_chat: topicOfChat,
            messages: [],
			room,
        }
		:
		{
            topic_of_chat: topicOfChat,
            messages: [],
			room,
        })
		:
		{
			tags,
			description,
			room,
            messages: [],
		}
        try{
            let chater_model = this._chater_model
			let chater_id = this._chater_id
			let socket = this._socket
			let delete_room = this.delete_room.bind(this)
			if(CACHED_IDS[chater_id] !== MAX_CHECKINGS_SEARCH ? await this.check_search(token) : false) return
			await this._model.createCollection()
                //start transaction of search
                await Acid( async (session) => {
					Log.debug('find neighbor')
					let neighbor = (_id === undefined ? !room : room && typeof _id == 'string') && await chater_model.findOneAndUpdate(
                        !room ?
						(topicOfChat !== 'fast_search' ?
						{
                            chater_id: {$ne: chater_id},
                            chater_sex: {$in: neighborSex},
                            chater_age: {$gte: neighborAgeFrom, $lte: neighborAgeTo},
                            neighbor_age_from: {$lte: chaterAge},
                            neighbor_age_to: {$gte: chaterAge},
                            neighbor_sexes: chaterSex,
                            topic_of_chat: topicOfChat,
                            state_chat: 1,
							room,
                        }
						:
						{
                            chater_id: {$ne: chater_id},
							topic_of_chat: topicOfChat,
							state_chat: 1,
							room,
						})
						:
						{
							_id: mongoose.Types.ObjectId(_id),
							state_chat: 1,
							room,
						}
						,
                        {
                            state_chat: 2,
                            neighbor_id: chater_id,
                        }, {new: true, session})
                        .select('chater_id user_id')
                    //deleting images from last chat
					Log.debug('find a chater')	
                    let chater = await chater_model.findOneAndUpdate(
                    {
                        chater_id,
						state_chat: {$in: [0, 3]},
					},
					{
						$unset: {
							room_id: '',
						},
						state_chat: 0,
						user_id: socket.request.user._id,
					}
					,{session, upsert: true, new: true})
					.select('images room_id')

					if(chater.room_id) {
						Log.debug('delete room')
						await delete_room(session)
					}
					Log.debug('search neighbor')
					if(room === true && typeof _id === 'string' && !neighbor) {
						this._io.in(this._socket.id).emit('DO_NOT_DISTURB')
					}
                    else if(neighbor && (_id === undefined ? room === false : room && _id)) {
						Log.info('create room')
						//create a room
						let room = await this._model.create([{
							members: [chater_id, neighbor.chater_id]
						}],
						{
							session,
						})
						Log.info('room is created: ', room[0]._id)
                        //update chater to chating state
                        await chater_model.updateOne(
							{
								chater_id,
								state_chat: {$in: [0,3]},
							},
							{
								state_chat: 2,
								neighbor_id: neighbor.chater_id,
								room_id: room[0]._id,
								...dataChater 
							}, {new: true, session})
						await chater_model.updateOne(
							{
								chater_id: neighbor.chater_id,
							},
							{
								room_id: room[0]._id,
							},
							{session}
						)
					
						if(!publisher.connected) throw 'Redis is not connected'

						this.subscribe()
						//set the room id
						this._room_id = room[0]._id
						socket.join(this._room_id)
						this._io.in(chater_id).in(neighbor.chater_id).emit('SET_STATE_CHAT', 2)
						this._io.in(neighbor.chater_id).clients((err , clients) => {
							for(let key of clients) {
								this._io.sockets[key]._events['SET_ROOM_ID'](room[0]._id.toString())
							}
						})
						if(socket.request.user.logged_in) {
							this._io.in(neighbor.chater_id).emit('SET_ADD_FRIEND_ID', socket.request.user._id)
							this._io.in(chater_id).emit('SET_ADD_FRIEND_ID', neighbor.user_id || '')
						}
                    }
                    else {
                        //update chater to searching state
                        await chater_model.updateOne(
                        {
                            chater_id: chater_id,
                            state_chat: {$in: [0,3]},
                        },
                        {
                            state_chat: 1,
                            neighbor_id: '',
                            ...dataChater
                        }, {new: true,session})
                        this._io.in(chater_id).emit('SET_STATE_CHAT', 1)
                    }
                })
        }
        catch(e) {
            Log.error(e)
        }
        
    }
	on_set_room_id(id) {
		let socket = this._socket
		socket.join(id)
		this._room_id = mongoose.Types.ObjectId(id)
	}
    async on_return_to_data() {
        let io = this._io
		let chater_id = this._chater_id
		let chater_model = this._chater_model
		let delete_room = this.delete_room.bind(this)
		await Acid(async (session) => {
			let chater = await chater_model.findOneAndUpdate(
				{
					chater_id: chater_id,
					state_chat: {$in: [1, 3]},
				},
				{
					state_chat: 0,
					images: [],
					messages: [],
				},
				{session}
			)
			await delete_room(session)
			if(chater) io.in(chater_id).emit('SET_STATE_CHAT', 0)
		})
    }
    async on_stop_chat() {
        let chater_model = this._chater_model
		let chater_id = this._chater_id
        let io = this._io
        await Acid(async (session) => {
            let chater = await chater_model.findOneAndUpdate(
                {
                    chater_id,
                    state_chat: 2,
                },
                stateStopChat,
                {session}
            )
            .select('neighbor_id')

            let neighborId = chater.neighbor_id
            await chater_model.updateOne(
                {
                    chater_id: neighborId,
                    state_chat: 2,
                },
                stateStopChat,
                {session}
			)
            io.in(chater_id).in(neighborId).emit('SET_STATE_CHAT', 3)
        })        
	}
    on_disconnect(chater_id){

		if(!chater_id.match(/chater_/))  super.on_disconnect()
		let chater_model = this._chater_model
        let io = this._io
		chater_id = chater_id.match(/chater_/) ? chater_id : this._chater_id
		let delete_room = this.delete_room.bind(this)
		this.count_users()
		Log.info("on disconect chater id:", chater_id)
        setTimeout(async () => {
            if(io.adapter.rooms[chater_id] === undefined) {
				await Acid(async (session) => {
                    let chater = await chater_model.findOne({
                        chater_id,
                    },
                    'state_chat neighbor_id room_id'
                    )
					if(chater !== null) {
						if(chater.state_chat == 2) {
							io.in(chater.neighbor_id).emit('SET_STATE_CHAT', 3)
						}

						await chater_model.updateOne(
							{
								chater_id: chater.neighbor_id,
								state_chat: 2,
							},
							stateStopChat,
							{session}
						)
						await chater_model.deleteOne({chater_id}, {session})
						await delete_room(session, chater.room_id)
					}
                })
            }
        }, TIME_OUT_DESTROY)	
    }
    count_users() {
        let io = this._io
		let chater_id = this._chater_id
		let rooms = io.adapter.rooms[chater_id]
		if(rooms && !rooms.sockets.hasOwnProperty(chater_id) && !CHATER_ROOMS.includes(chater_id)) {
			CHATER_ROOMS.push(chater_id)
			io.emit('COUNT_USERS', CHATER_ROOMS.length)
		}
		else if(rooms === undefined) {
			if(CHATER_ROOMS.indexOf(chater_id) !== -1) CHATER_ROOMS.splice(CHATER_ROOMS.indexOf(chater_id), 1)
			io.emit('COUNT_USERS', CHATER_ROOMS.length)
		}
		else {
			io.in(this._socket.id).emit('COUNT_USERS', CHATER_ROOMS.length)
		}
	}
	async delete_room(session, room_id) {
		let model = this._model
		room_id = room_id ? room_id : this._room_id
		let chater_id = this._chater_id
		let socket = this._socket
		let images = await model.aggregate([
			{
				$match: {
				_id: room_id,
				},
			},
			{
				$unwind: '$messages'
			},
			{
				$match: {
					'messages.type': 'image',
				}
			},
			{
				$project: {
					path_to : '$messages.body_message'
				}	
			}
		])
		
		let deleted_room = await model.deleteOne({
			_id: room_id,
			members: {
				$size: 1,
			}
		},
		{session}
		)
		await model.updateOne({
			_id: room_id,
		},
		{
			$pull: {
				members: chater_id,
			},
		},
		{session}
		)
		socket.leave(room_id)
		if(deleted_room.n) {
			for(let i in images) {
				Fs.unlink('/var/www/chat/backend'+images[i].path_to, (err) => console.error(err))
			}
		}
	}
}

let onConnect = function(CHAT) {
	return  async function(socket) {
		let chatIO = new ChatIO(CHAT, socket, Room.chater_room, Chater)
		//this code cleans junk chaters after rebooting system or reloading app
		if(CLEANING_OF_JUNK_CHATERS) {
			CLEANING_OF_JUNK_CHATERS = false
			let chater_ids = await Chater.distinct('chater_id')
			Log.info('chater_ids for deleting: ', chater_ids)
			for(let id of chater_ids) {
				chatIO.on_disconnect(id)
			}
		}	
	}
}
module.exports = function(socket) {
	socket.on('connection',onConnect(socket))
}
