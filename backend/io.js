const cookieParser = require('cookie-parser')
const passportSocketIo = require("passport.socketio")
const ExpressSession 	= require('express-session')
const FileStore 		= require('session-file-store')(ExpressSession)
let FileStoreOptions 	= {
	logFn: () => true,
}

module.exports = function(io) {
	//check host
	io.use((socket, next)=>{
	  let verifiedHosts = ['apogovorim.ru', 'localhost']
	  let host = socket.handshake.headers.host
	  if(verifiedHosts.includes(host)){
		next()
	  }
	})
	//namespaces
	let chat = io.of('/chat')
	let conversation = io.of('/conversation')
	let notification = io.of('/notification')		
	socketAuth(conversation)
	socketAuth(notification)
	socketAuth(chat, chatAuthSuccess, chatAuthFail)

	function socketAuth(s, success, fail) {
		let opts = {
			cookieParser: cookieParser,       // the same middleware you registrer in express
			key:          'connect.sid',       // the name of the cookie where express/connect stores its session_id
			secret:       'pureLab',    // the session_secret to parse the cookie
			store:        new FileStore(FileStoreOptions),        // we NEED to use a sessionstore. no memorystore please
			// success:      onAuthorizeSuccess,  // *optional* callback on success - read more below
			// fail:         onAuthorizeFail,     // *optional* callback on fail/error - read more below
		}
		if(success) opts.success = success
		if(fail) opts.fail = fail
		s.use(passportSocketIo.authorize(opts))
	}
	function chatAuthSuccess(data, accept){
		// The accept-callback still allows us to decide whether to
		// accept the connection or not.]
		accept(null, true);
	}
	   
	function chatAuthFail(data, message, error, accept) {
		// We use this callback to log all of our failed connections.
		accept(null, true);
	}

	//io controller
	require('./io/conversation')(conversation)
	require('./io/notification')(notification)
	require('./io/chat')(chat)
}