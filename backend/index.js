(async()=>{
const Log     = require('./utils/log')
try{
  const env     = require('dotenv').config({
    path: '/var/www/chat/backend/.env',
  }).parsed
  require('events').EventEmitter.defaultMaxListeners = Infinity
  let chater = require('./db/models/friend')
  let room = require('./db/models/room')
  // chater.remove({}, err=> {
  //   console.log('chater is empty')
  //   console.log(err)
  // })
  // room.user_room.updateMany({}, {messages: []}, err=> {
  //   console.log('room is empty')
  //   console.log(err)
  // })
  const App     = require('./app')() 
  const Server  = require('http').Server(App)
  const Io      = require('socket.io')(Server)
  
  require('./io')(Io) 

  Server.listen(env.PORT,()=>{Log.info('server started')}) 
}
catch(err){
  Log.error(err)
}})()