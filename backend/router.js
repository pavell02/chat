const AuthController 	= require('./controllers/authcontroller.js')
const MiddlewareController = require('./controllers/middleware.js')
const APIController = require('./controllers/APIcontroller.js')
const UploadController = require('./controllers/upload_controller.js')
const UserController = require('./controllers/userconroller.js')
const FriendsController = require('./controllers/friendcontroller.js')
const UserRoomController = require('./controllers/user_room_controller')
const Passport	= require('passport')
const ChatController = require('./controllers/chat_controller')

module.exports = function(App) {
	//authenticate
	App.post('/registration', MiddlewareController.limitRequest(30), MiddlewareController.antibot, AuthController.registration)
	App.get('/confirm-email/:username/:salt', AuthController.confirmEmail)
	App.post('/login', MiddlewareController.limitRequest(15), MiddlewareController.antibot, Passport.authenticate('local'), AuthController.authenticated)
	App.get('/logout', AuthController.logout)
	App.get('/authenticated', AuthController.authenticated)
	App.get('/confirm-restore-password', MiddlewareController.limitRequest(30), MiddlewareController.antibot, AuthController.sendMessageToRestorePassword)
	App.post('/restore-password', MiddlewareController.limitRequest(30), AuthController.restorePassword)
	//api 
	App.get('/exists/:db/:field/:value', MiddlewareController.limitRequest(2), APIController.exists)
	//user
	App.get('/find-user', AuthController.isLogged, UserController.getUsers)
	//freinds
	App.get('/get-friends', AuthController.isLogged, FriendsController.getFriends)
	App.post('/add-friend', AuthController.isLogged, FriendsController.addFriends)
	App.post('/reject-friend', AuthController.isLogged, FriendsController.rejecteRequest)
	App.post('/accept-friend', AuthController.isLogged, FriendsController.acceptRequest)
	App.get('/count/friend', AuthController.isLogged, FriendsController.count)
	//chat
	App.post('/updload/:to/:type', MiddlewareController.checkFile, UploadController.updloadFile)
	App.get('/chat-rooms', ChatController.getRooms)
	//conversations
	App.get('/get-rooms', UserRoomController.getRooms)
	App.get('/count/conversation', AuthController.isLogged, UserRoomController.count)
}