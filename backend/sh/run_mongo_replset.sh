#!/bin/sh
echo 'run mongo replset'
sudo mongod --config /etc/mongod.conf --dbpath /var/lib/mongo/apogovorim_1 --port 27001 --replSet rs --fork --logpath /var/log/mongodb/apogovorim_1.log
sudo mongod --config /etc/mongod.conf --dbpath /var/lib/mongo/apogovorim_2 --port 27002 --replSet rs --fork --logpath /var/log/mongodb/apogovorim_2.log
sudo mongod --config /etc/mongod.conf --dbpath /var/lib/mongo/apogovorim_3 --port 27003 --replSet rs --fork --logpath /var/log/mongodb/apogovorim_3.log