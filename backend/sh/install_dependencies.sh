#install node and npm 
echo "install node and npm"
sudo apt update
sudo apt install nodejs
sudo apt install npm
npm -v
#install redis
echo "install redis";
sudo apt update
sudo apt install redis-server
sudo sed -i 's/supervised no/supervised systemd/' /etc/redis/redis.conf
sudo systemctl restart redis.service
sudo systemctl status redis
#install nginx
echo "install nginx";
sudo apt update
sudo apt install nginx
sudo systemctl status nginx
#install mongodb
echo "instlal mongodb";
sh ./install_mongodb.sh
#copy project
sudo cp -r ~/chat /var/www
