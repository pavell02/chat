#!/bin/bash
FOLDER=/etc/apogovorim
if [ -d "$FOLDER" ]
then 
    echo "password already was setted"
else
    echo "Enter a password for admin db or we will generate its automaticly:"
    read ADMIN_PASSWORD
    echo "Enter a password for apogovorim db or we will generate its automaticly:"
    read APOGOVORIM_PASSWORD
    if [ -z $ADMIN_PASSWORD ] 
    then
    ADMIN_PASSWORD=$(openssl rand -hex 30)
    fi
    if [ -z $APOGOVORIM_PASSWORD ] 
    then
    APOGOVORIM_PASSWORD=$(openssl rand -hex 30)
    fi
    sudo mkdir /etc/apogovorim 
    sudo echo "ADMIN_PASSWORD=$ADMIN_PASSWORD
    APOGOVORIM_PASSWORD=$APOGOVORIM_PASSWORD
    USER_DB=apogovorim
    " > /etc/apogovorim/apogovorim_db.env
    sudo mongo --port 27001 --eval "var adminPassrowd='$ADMIN_PASSWORD', apogovorimPassword='$APOGOVORIM_PASSWORD'" utils/secure.js
    sudo openssl rand -base64 756 > /etc/apogovorim/key
    sudo chmod 400 /etc/apogovorim/key
    sudo chown -R mongodb:mongodb /etc/apogovorim/key
    sudo sed -i '/security/s/^#//g' /etc/mongod.conf
    sudo sed -i 's+security:+security:\n    authorization: enabled\n    keyFile: /etc/apogovorim/key+g' /etc/mongod.conf
    sudo sed -i '/replication/s/^#//g' /etc/mongod.conf
    sudo sed -i 's+replication:+replication:\n    replSetName: rs+g' /etc/mongod.conf
    PORTS=(27003 27002 27001)
    for port in "${PORTS[@]}"
    do
        sudo mongo --port $port --eval "db.getSiblingDB('admin').shutdownServer({force: true})"
    done
    sudo ./run_mongo_replset.sh
fi