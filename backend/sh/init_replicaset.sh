sudo mkdir /var/lib/mongo
sudo mkdir /var/lib/mongo/apogovorim_1
sudo mkdir /var/lib/mongo/apogovorim_2
sudo mkdir /var/lib/mongo/apogovorim_3
sudo chown mongodb:mongodb /var/lib/mongo/apogovorim_1
sudo chown mongodb:mongodb /var/lib/mongo/apogovorim_2
sudo chown mongodb:mongodb /var/lib/mongo/apogovorim_3
sudo cp ./utils/run-replset.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable run-replset.service
sudo systemctl start run-replset
sudo systemctl status run-replset
sudo echo "Проверяем, стартанули ли все экземпляры:"
sudo ps aux | grep mongo| grep -Ev "grep"
sudo mongo --host 127.0.0.1 --port 27001 < utils/mongo.js
