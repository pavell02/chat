print('setting passwords')
print('admin:' + adminPassrowd)
print('apogovorim:' + apogovorimPassword)
db.getSiblingDB("admin").dropUser('admin')
db.getSiblingDB("admin").createUser(
{
    user: "admin",
    pwd: adminPassrowd, // or cleartext password
    roles: [ 
        { 
            role: "userAdminAnyDatabase",
            db: "admin"
        },
        "readWriteAnyDatabase" 
    ]
}
)
print('admin password is setted')
db.getSiblingDB("apogovorim").dropUser('apogovorim')
db.getSiblingDB("apogovorim").createUser(
{
    user: "apogovorim",
    pwd:  apogovorimPassword,   // or cleartext password
    roles: [ 
        { 
            role: "readWrite",
            db: "apogovorim"
        },
        {
            role: "read",
            db: "reporting"
        }
    ]
}
)
print('apogovorim password is setted')