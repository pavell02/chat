const mongoose 	=	require('mongoose') 
const bcrypt 	=	require('bcrypt') 
const envCommon		=	require('dotenv').config({
    path: '/var/www/chat/backend/.env',
}).parsed
const envApogovorim	=	require('dotenv').config({
    path: '/etc/apogovorim/apogovorim_db.env',
}).parsed
const log 		= require('../utils/log')
mongoose.set('useFindAndModify', false) 
mongoose.set('useNewUrlParser', true)
mongoose.set('useCreateIndex', true)
//open connection
async function connect() {
	try {
		let url = `mongodb://${envApogovorim.USER_DB}:${envApogovorim.APOGOVORIM_PASSWORD}@localhost:${envCommon.PORT_DB}/${envCommon.NAME_DB}`
		//log.info('trying to connect to db: ' + url)
		await mongoose.connect(
			url, 
			{
				useNewUrlParser: true,
				replicaSet: 'rs',
			}
		)
		log.info('mongodb is connected successfully')
	}
	catch(err) {
		log.error(err)
		setTimeout(connect, 5000)
	} 
}
connect()
//set object shema
module.exports = mongoose.Schema 
