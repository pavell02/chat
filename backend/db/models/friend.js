const mongoose = require('mongoose')
const Schema = require('../schema.js')
//types 
const types = {
	refToUser: {
		type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
	},
	status: {
        type: Number,
        required: true,
        enums: [
            0,    //'add friend',
            1,    //'requested'
        ]		
	},
}
//set shema user
const friendsSchema = new Schema({
    requester: types.refToUser,
    recipient: types.refToUser,
    status: types.status,
}, {timestamps: true})
module.exports = mongoose.model('friends', friendsSchema)