const mongoose = require('mongoose')
const Schema = require('../schema.js')
//types 
const types = {
	uniqueStr: {
		type: String,
		unique: true,		
	},
}
//set image shema
const imageShema = new Schema({
	_id: 	Schema.Types.ObjectId,
	path: 	types.uniqueStr,
})

module.exports = mongoose.model('image',imageShema)