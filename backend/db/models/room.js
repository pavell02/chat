const mongoose = require('mongoose')
const Schema = require('../schema.js')
//types 
const types = {
    string: {
        type: String,
        required: true,
    },
    boolean: {
        type: Boolean,
        required: true,
    },
    refToUser: {
		type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
	}
}
//default schema 
const defaultRoom = {
    members: [types.string],
    messages: [
        {
            "whose" : types.string,
            "type" : {
                ...types.string,
                enum: ['image', 'text'],
            },
            "time" : {
                ...types.string,
                default: Date.now(),
            },
            "read" : {
                ...types.boolean,
                default: false,
            },
            "body_message" : types.string,
        }
    ],
}
//set shema room
const chaterRoomSchema = new Schema(defaultRoom)
const userDefaultRoom = {
    ...defaultRoom
}
userDefaultRoom.members = [types.refToUser]
userDefaultRoom.messages[0].whose = mongoose.Types.ObjectId
const userRoomSchema =  new Schema(userDefaultRoom)
module.exports = {
    chater_room: mongoose.model('chater_room',chaterRoomSchema),
    user_room: mongoose.model('user_room', userRoomSchema)
}