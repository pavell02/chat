const mongoose = require('mongoose')
const Schema = require('../schema.js')
//types 
const types = {
	uniqueStr: {
		type: String,
		unique: true,		
	},
	refToImg: {
		type: 	Schema.Types.ObjectId,
		ref: 	'image',
	},
	date: {
		type: Date,
		default: Date.now
	}
}
//set shema user
const userShema = new Schema({
	username: 	types.uniqueStr,
	password: 	String,
	email: 		types.uniqueStr,
	salt: 		String,
	active: 	Boolean,
	rights: 	String,
	//images: 	[types.refToImg],
	autoLoginHash: String,
	created_at: types.date,
	restore_password_to: Date,
})
module.exports = mongoose.model('user',userShema)