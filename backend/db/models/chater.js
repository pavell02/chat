const mongoose = require('mongoose') 
const Schema = require('../schema.js')
//types 
const types = {
	age: {
		type: Number,
		min: 14,
		max: 99,
		default: 18
	},
	sex: {
		type: String,
		enum: [ 'male', 'female'],
	},
	stateChat: {
		type: Number,
		enum: [ 0, 1, 2, 3],
		default: 0,		
	}
}
//set shema chaters
const chaterShema = new Schema({
	chater_id: {
		type: String,
		unique: true,
		require: true,
	},
	user_id: String,
	neighbor_id: String,
	socket_ids: [String],
	chater_age: types.age,
	chater_sex: types.sex,
	neighbor_age_from: types.age,
	neighbor_age_to: types.age,
	neighbor_sexes: [types.sex], 
	state_chat: types.stateChat,
	topic_of_chat: {enum: ['flirt', 'talk', 'fast_search']},
	tags: [String],
	description: String,
	room: Boolean,
	image_names: [String],
	"room_id": {
		type: Schema.Types.ObjectId,
		ref: 'rooms',
	}
})

module.exports = mongoose.model('chater',chaterShema)