const User 	    =	require('../db/models/user.js')
const Bcrypt 	= 	require('bcrypt') 
const Crypto 	=	require('crypto')
const Log 		= 	require('../utils/log')
strategies = {}
strategies.local = (username,password,done) =>{
	User.findOne({
			$or: [{	username}, {email: username}],
			active: 	true,
		},(err,doc) =>{
			Log.debug(doc)
			if(err) redirect('/')

			if(doc != null)	{
			
				let valid = Bcrypt.compareSync(password, doc.password)

				if(valid) {
					Log.debug('authenticated')
					done(null,{
						_id: doc._id,
						username: doc.username,
						rights: doc.rights,
					})
				}
				else {
					Log.debug('pas is not valid')
					done(null,false)
				}
			}
			else{
				Log.debug('doc is null')
				done(null,false)
			} 
		})
}
strategies.rememberMePart1 = (token,done)=>{
			//find user with this token
			User
			.findOne({
			    autoLoginHash: token,//params of search
            },
            (err,doc)=>{
            	if(err) done(err)
            		else if(!doc) done(null,false)
            			else{
            				//delete token for protection
            					delete doc.autoLoginHash
                                doc.save(()=> {})
                                done(null, {
									_id: doc._id,
									username: doc.username,
									rights: doc.rights,
								})
            			}
            })
		}
strategies.rememberMePart2 = (doc,done)=>{
			//genereate new token
			let token = Crypto.randomBytes(32).toString('hex')
			doc.autoLoginHash = token
			//save new token
            doc.save(()=> {})
            done(null, token)
}
module.exports = strategies