const Local 	= 	require('passport-local').Strategy
const RememberMe	=	require('passport-remember-me').Strategy
const Strategies = require('./strategies.js')
 
module.exports = (passport) =>{
	//create localstrategy
	passport.use(new Local(Strategies.local)) 
	//strategy remember me
	passport.use(new RememberMe({
			key:'token'
		},
		Strategies.rememberMePart1,
		Strategies.rememberMePart2
	))

	passport.serializeUser((user,done)=>{
		done(null,user)
	}) 
	passport.deserializeUser((user,done)=>{
		done(null,user)
	}) 
}