const fs = require('fs')
module.exports = function(path) {
	return (req, res, next) => {
		let replacement = {
			csrfToken: req.csrfToken()
		}
		if(fs.existsSync(`${path}${req.url}`) && req.url != '/') {
			res.sendFile(`${path}${req.url}`)
		}
		else {
			let spaFile = fs.readFileSync(`${path}/index.html`).toString()
			for(let key in replacement) {
				spaFile = spaFile.replace(`<{${key}}>`, replacement[key])
			}
			res.send(spaFile)
		}
	}
}