function log(state, ...args) {
    let time = new Date().toGMTString()
    console.log(time, state, ...args)
}

function debug(...args) {
    log('[DEBUG]', ...args)
}

function error(...args) {
    log('[ERROR]', ...args)
}

function info(...args) {
    log('[INFO]', ...args)
}

module.exports = {
    debug,
    error,
    info
}