const Express = require('express') 
const App 	  = Express() 
//const jsonParser = Express.json() 
const Csrf 	  = require('csurf')
//const Ddos	  = require('ddos')

const Passport 			= require('passport') 
const ExpressSession 	= require('express-session')
const FileStore 		= require('session-file-store')(ExpressSession)
const Cookie 			= require('cookie-parser') 
const Body 				= require('body-parser')
const FileUpload 		= require('express-fileupload')


//const passportSt 		= require('./passport-strategy.js')
const Middleware		= require('./controllers/middleware.js')
const SpaRouter			= require('./utils/spa-router.js')
//const ddos 				= new Ddos({burst:10, limit:500, maxcount:5})
let FileStoreOptions 	= {
	logFn: () => true
}

	//App.use(Express.static(__dirname + "/static")) 

	App.use(ExpressSession({
		store: new FileStore(FileStoreOptions),
		secret: 'pureLab',
		resave: false,
    	saveUninitialized: false,
    	httpOnly: true,
    	cookie:{
    		expires: new Date(Date.now() + 1000*60*60*24*365),
    	}
	})) 
	//App.use(ddos.express)
	App.use(Cookie()) 
	App.use(Body.urlencoded({ extended: true })) 
	App.use(Body.json()) 
	App.use(Csrf({ cookie: true }))
	App.use(FileUpload())

	App.use(Passport.initialize()) 
	App.use(Passport.session())
	App.use(Passport.authenticate('remember-me'))

	//auth
	require('./passports')(Passport)

	App.use(Middleware.addUniqId)
	//router
	require('./router.js')(App)
	// Handle SPA
	App.use(SpaRouter(__dirname.replace('backend','frontend') + "/dist"))

	//get routs
	//chat rout
	// App.get('/',CsrfProtection,userController.chat)

	// //route regestration
	// App.get('/singup',CsrfProtection,userController.singup) 

	// //route check that user is actived
	// App.get('/check-active/:nick/:salt',userController.check) 

	// //logout
	// App.get('/logout',userController.logout)
	// //galary
	// App.get('/gallery',CsrfProtection,userController.gallery) 
	// //settings
	// App.get('/settings',CsrfProtection,userController.settings)
	// //rules
	// App.get('/politika',CsrfProtection,userController.politika)

	// App.get('/agreement',CsrfProtection,userController.agreement)

	// App.get('/cookie',CsrfProtection,userController.cookie)
	// //log of errors
	// App.get('/error',CsrfProtection,utilAdmin.rights,admincontroller.errors)
	// //page of users
	// App.get('/users/:page',CsrfProtection,utilAdmin.rights,admincontroller.pageUsers)
	// //redirect to users
	// App.get('/users',CsrfProtection,utilAdmin.rights,admincontroller.redirectPageUsers)
	// //list of users
	// App.get('/get-users',CsrfProtection,utilAdmin.rights,admincontroller.getUsers)

	// //post routs

	// //register
	// App.post('/register',CsrfProtection,userController.register) 

	// //login
	// App.post('/login',CsrfProtection,passport.authenticate('local', {failureRedirect: '/singup?fail=true',successRedirect: '/' })) 

	// App.post('/checklogin',jsonParser,CsrfProtection,userController.isNick) 

	// App.post('/checkemail',jsonParser,CsrfProtection,userController.isEmail) 

	// App.post('/save_to_gallery',jsonParser,CsrfProtection,chatController.add_to_gallery)

	// //load an image from ajax form
	// App.post('/photo/save',CsrfProtection,chatController.photo_save) 

	// //send photo to user from his collection
	// App.post('/collection',userController.collection)

	// //settings

	// //change login
	// App.post('/change-login',CsrfProtection,settingsController.changelogin)

	// //change password
	// App.post('/change-password',CsrfProtection,settingsController.changepassword)

	// App.use(function(req, res, next) {
 //  		res.status(404).send('404')
	// }) 
	
module.exports = function() {
	return App 
}