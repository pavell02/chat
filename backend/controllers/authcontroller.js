const Crypto 	= require('crypto-random-string')
const User 		= require('../db/models/user.js')
const Bcrypt	= require('bcrypt')
const nodemailer = require('nodemailer')
const env     = require('dotenv').config({
    path: '/var/www/chat/backend/.env',
}).parsed
const log 		= require('../utils/log')
const transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
	  user: 'apogovorim.s@gmail.com',
	  pass: 'peresmeshnick1110987654321'
	}
})

let AuthController = {}

AuthController.registration = (req,res,next) => {

	let auth 	= req.isAuthenticated()

	if(auth) res.redirect('/')

	let username	= req.body.username 
	let email	= req.body.email 
	let pas 	= req.body.password

	//check username
	check  = /^[A-Za-z0-9_-]{4,20}$/.test(username) && username.length != 0 
	//chekc pas
	check = check && /^\S+$/.test(pas) 
	//chekc lenght pas
	check = check && (pas.length >= 6)
	//if data was wrong redirect route to singup
	if(!check)  return res.json({message: 'Неверно введены данные.'}) 

	//for this salt i'll check that user was activated
	let salt = Crypto({length: 35, type: 'url-safe'}) 

	let htmlMessage = 'Добрый день, чтобы продтвердить свой аккаунт перейдите по  <a href = "http://'+env.DOMAIN+'/confirm-email/'+username+'/'+salt+'">ссылке</a>.\nЕсли письмо пришло по ошибке удалите его. После подтверждения аккаунта вы сможете авторезироваться.Не отвечайте на это сообщение.' 
	

	let user = new User({
		username: 	username,
		email: 	email,
		active: false,
		salt,	
	})


	let saltRounds = 10;
	salt = Bcrypt.genSaltSync(saltRounds)
	let hash = Bcrypt.hashSync(pas, salt)
	
	user.password = hash

	transporter.sendMail({
		from: '"Apogovorim no-reply" <apogovorim.ru@gmail.com>',
		subject: "",
		to:   email,
		html: htmlMessage,
		type: 'text/html',
	  }, 
		  (err, reply) =>{
			if(err) {
				log.error('err mail',err && err.stack)
				let message = 'Что-то пошло не так попробуйте еще раз позже.'
				return res.json({message})
			}
			else {
				log.debug('reply mail',reply)
				user.save(err => {
					if(err){
						let message = 'Что-то пошло не так попробуйте еще раз позже.'
						return res.json({message})
					}
					else {
						res.json({message: 'Вам на почту пришло сообщение для подтверждения email. Если нет ПРОВЕРЬТЕ СПАМ или попробуйте еще раз позже.'})
						log.debug(htmlMessage)
					}
				})
			}
		}
	)
}

AuthController.confirmEmail = (req,res) => {
	let auth 	= req.isAuthenticated()

	if(auth) res.redirect('/')
		
	let username = req.params['username'] 
	let salt = req.params['salt'] 

	User.updateOne(
		{
			username: username,
			salt: salt,
		},
		{
			active:true,
			salt: null,
		},
		(err,doc)=>{
			if(err) log.error(err)
			if(doc.nModified == 1)	res.redirect('/alert-message?message=Email подтвержден. Можете авторизироваться.&path=/')
			else res.redirect('/alert-message?message=Что-то пошло не так. Попробуйте еще разmessage=&path=/') 
		}
	) 
}

AuthController.logout = (req, res) => {
  req.logout()
  res.redirect('/')
}

AuthController.sendMessageToRestorePassword = async (req, res) => {
	let email = req.query.email
	let salt = Crypto({length: 35, type: 'url-safe'})
	let restoreTime = new Date()
	restoreTime.setMinutes(restoreTime.getMinutes() + 3) // timestamp
	restoreTime = new Date(restoreTime) // Date object
	let message = 'Что-то пошло не так попробуйте позже.'
	try{
		let user = await User.findOneAndUpdate({
			email,
			active: true,
		},
		{
			salt,
			restore_password_to: restoreTime,			
		})
		if(user) {
			let htmlMessage = 'Добрый день, если ли вы хотите переустановить пароль перейдите по этой ссылке  <a href = "http://'+env.DOMAIN+'/restore-password/'+user.username+'/'+salt+'">ссылке</a>.\nЕсли письмо пришло по ошибке удалите его. Не отвечайте на это сообщение.' 
			log.debug(htmlMessage)
			transporter.sendMail({
				from: '"Apogovorim no-reply" <apogovorim.ru@gmail.com>',
				subject: "",
				to:   email,
				html: htmlMessage,
				type: 'text/html',
			}, 
				(err, reply) =>{
					if(err) {
						log.error('err mail',err && err.stack)
						res.json({message: 'Что-то полшло не так попробуйте еще раз позже.'})

					}  
					else {
						log.debug('reply mail',reply)
						res.json({message: 'Вам на почут пришло письмо для смены пароля. Если нет ПРОВЕРЬТЕ СПАМ или попробуйте еще раз позже.'})
					}
				}
			)
		}
		else {
			res.json({message})
		}
	}
	catch(err) {
		log.error(err)
		res.json({message})
	}
}

AuthController.restorePassword = async (req, res) => {
	let username 	= req.body.username
	let salt 		= req.body.salt
	let password 	= req.body.password
	try{
		let user = await User.findOne({
			username,
			salt,
		})

		if(user === null) {
			return res.json({message: 'Ссылка не действительна'})
		}

		let limitTime = new Date(user.restore_password_to).getTime()

		if(Date.now() < limitTime) {
			let saltRounds = 10
			salt = Bcrypt.genSaltSync(saltRounds)
			let hash = Bcrypt.hashSync(password, salt)
			let data = await User.updateOne({
				username
			},
			{
				salt: null,
				password: hash,
			})	
			return res.json({message: 'Пароль успешно изменен'})		
		}
		else {
			return res.json({message: 'Время для изменения пароля истекло, попробуйте еще раз'})
		}
	}
	catch(err) {
		log.error(err)
		res.json({message: 'Что-то пошло не так.'})
	}
}

AuthController.isLogged = function(req, res, next) {
	if(req.isAuthenticated()) {
		next()
	}
	else {
		return res.status(401)
	}
}

AuthController.authenticated = (req, res) => {
	res.json({auth: req.isAuthenticated()})
}
module.exports = AuthController