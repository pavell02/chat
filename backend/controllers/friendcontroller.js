const Friend 		= require('../db/models/friend.js')
const User 			= require('../db/models/user.js')
const Room 			= require('../db/models/room.js')
const Mongoose      = require('mongoose')
const Acid			= require('mongoose-acid')
const Log			= require('../utils/log')


const FriendsController = {}
FriendsController.addFriends = async (req, res) => {
	const requester = req.session.passport.user._id
	const recipient = req.body.recipient
	if(requester == recipient) return res.json({message: 'Вы не можете отправлять себе запрос'})
	try {
		let check = await Friend.findOne({
			$or: [
				{ requester, recipient },
				{
					requester: recipient,
					recipient: requester,
				}
			]
		})
		if(check) {
			return res.json({message: 'Заявка уже существует.'})
		}
		await Friend.findOneAndUpdate(
		    { 
				requester,
				recipient
			},
		    {
				$set: {
					status: 0
				}
			},
		    { upsert: true, new: true }
		)
		res.json({
			message: 'Запрос отправлен.'
		})
	}
	catch(err) {
		Log.error(err)
		res.json({success: false})
	}
}

FriendsController.acceptRequest = async (req, res) => {
	const recipient = Mongoose.Types.ObjectId(req.session.passport.user._id)
	const requester = Mongoose.Types.ObjectId(req.body.requester)
	try {
		Acid(async (session) => {
			await Friend.findOneAndUpdate(
				{ 
					requester,
					recipient,
					status: 0,
				},
				{ $set: { status: 1 }},
				{session}
			)
			await Room.user_room.updateOne({
				members:{
					$all: [
						{$elemMatch: { $eq:recipient}},
						{$elemMatch: { $eq:requester}},
					]
				}
			},
			{
				$set: {
					members: [recipient, requester],
				}
			},
			{
				upsert: true,
				session
			})
			res.json({
				success: true
			})
		})
	}
	catch(err) {
		Log.error(err)
		res.json({success: false})
	}
}

FriendsController.rejecteRequest = async (req, res) => {
	const recipient = Mongoose.Types.ObjectId(req.session.passport.user._id)
	const requester = Mongoose.Types.ObjectId(req.body.requester)
	try {
		await Friend.findOneAndRemove(
		    { requester, recipient}
		)
		res.json({
			success: true
		})
	}
	catch(err) {
		Log.error(err)
		res.json({success: false})		
	}	
}

FriendsController.getFriends = async (req, res) => {
	let _id = req.session.passport.user._id
	let status = parseInt(req.query.status)
	let limit = parseInt(req.query.limit) || 30
	let offset = parseInt(req.query.offset) || 0
	let page = parseInt(req.query.page) || 0
	let username = req.query.username
	try {
		//ger links to friends
		let friends = await Friend.aggregate([
			{
				$match: {
					$or: status === 0? 
					[
						{recipient: Mongoose.Types.ObjectId(_id)}
					]
					:
					[
						{requester: Mongoose.Types.ObjectId(_id)},
						{recipient: Mongoose.Types.ObjectId(_id)}
					],
					status,
				},
			},
			{
				$addFields: {
					lookup_user_id: {
						$cond: {
							if: status == 0 || {
								$ne:["$requester", Mongoose.Types.ObjectId(_id) ]
							},
							then: "$requester",
							else: "$recipient"
						}
					},
				}
			},
			{
				$lookup: {
					from: "users",
					localField: 'lookup_user_id',
					foreignField: "_id",
					as: "user"
				}
			},
			{
				$match: {
					'user.username': {
						$regex: username,
					},
				}
			},
			{ 
				$skip : limit * page
			},
			{ 
				$limit : limit + 1
			},
			{
				$lookup: {
					from: "user_rooms",
					let: {
						requester: "$requester",
						recipient: "$recipient"
					},
					pipeline: [
						{
							$match: {
								$expr: {
									$and: [
										{'$in':["$$requester", {$ifNull :['$members',[]]}]},
										{'$in':["$$recipient", {$ifNull :['$members',[]]}]},
									]
								}							
							}
						},
						{
							$project: {
								_id: '$_id'
							}
						}
					],	
					as: "room_id"
				}
			},
			{
				$project: {
					user: { 
						$arrayElemAt: ['$user', 0]
					},
					room_id: { 
						$arrayElemAt: ['$room_id', 0]
					},
				}
			},
			{
				$project: {
					_id: '$user._id',
					username: '$user.username',
					status: '$user.status',
					avatar: '$user.avatar',
					room_id: '$room_id._id',
				}
			}
		])
		res.json(friends)
	}
	catch(err) {
		Log.error(err)
		res.json({success: false})		
	}
}
FriendsController.count = async (req, res) => {
	let _id = req.session.passport.user._id
	try {
		const count = await Friend.countDocuments({
			recipient: Mongoose.Types.ObjectId(_id),
			status: 0,
		})
		res.json({count})
	}
	catch(err) {
		Log.error(err)
		res.json({message: 'smt went wrong'}).status(500)		
	}
}
module.exports = FriendsController