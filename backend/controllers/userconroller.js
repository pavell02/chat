const User 		= require('../db/models/user.js')  
const Log 		= require('../utils/log')
let   userController = {} 
const USER_SELECT = '_id username status avatar'

userController.collection = (req,res)=>{

	let auth = req.isAuthenticated()

	if(!auth) res.end()//if user isn't auth

	let username = req.session.passport.user.username
	let step = req.body.step

	step = parseInt(step,10)


	if(typeof step != 'number') res.end()//if data was wrong

	let take = 20
	let skip = step*take

	user.findOne({
		username: username,
	},
	{
		images:{
			$slice:[skip,take],//needed array of photo's path
		}
	}).populate('images').exec((err,doc)=>{

		if(err){
			Log.error(err)
			res.end()
		} 


			let arrTemp = []//save a names of files

			for(let image of doc.images){

				arrTemp.push(image.path)
			
			}


			res.send(arrTemp)//send paths
	})


}


userController.getUsers = async (req, res) => {
	let _id = req.session.passport.user._id
	let limit = parseInt(req.query.limit) || 30
	let offset = parseInt(req.query.offset) || 0
	let page = parseInt(req.query.page) || 0
	let username = req.query.username
	try {
		let users = await User
					.find({
						_id: {
							$ne: _id,
						},
						username: {
							$regex: username,
						},
						active: true,
					})
					.select(USER_SELECT)
					.skip(limit * page)
					.limit(limit + 1)
		res.json(users)
	}
	catch(err) {
		console.error(err)
		res.json({err: 'Что-то пошло не так'})
	}
}

module.exports = userController 