const crRand = require('crypto-random-string')
const Got		= require('got')
const Log = require('../utils/log')
const ENV = require('dotenv').config({
	path: '/var/www/chat/backend/.env',
}).parsed
const middleware = {}

middleware.addUniqId = async (req, res, next) => {
	if(!req.cookies.chater_id) {
		let chater_id = crRand({
			length: 25,
			type: 'base64',
		})
		res.cookie('chater_id', 'chater_' + chater_id, {
			expires: new Date(Date.now() + 1000*60*60*24*365)
		})
		next()
	}
	else {
		next()
	}	
}

middleware.limitRequest = (limitTime) => {
		return (req, res, next) => {
		let cookie = parseInt(req.cookies[req.route.path]) //last time request
		let now = Math.round(Date.now() / 1000)
		if(cookie && now < cookie) {
			let diff = cookie - now
			res.json({message: `Лимит превышен. Попробуйте через ${diff}c.`})
		}
		else { 
			res.cookie(req.route.path, now + limitTime, {httpOnly: true,})
			next()
		}
	}
}

middleware.checkFile = (req, res, next) => {
	if (!req.files || Object.keys(req.files).length === 0) {
		return res.status(400).json({message: 'Не было загружено файлов.'});
	}
	let type = req.params['type']
	let imgs	= ["image/jpeg", "image/gif", "image/png"]
	//check types
	let types = ['image']
	if(!types.includes(type)) return res.status(400).json({message: 'Incorrect file type'})
	//check image mimetype
	if(type === 'image') {
		for(let key in req.files) {
			if(!imgs.includes(req.files[key].mimetype)) {
				return res.status(400).json({message: `Файл ${req.files[key].name} имеет не корректный тип.`})
			}
		}
	}
	
	next()
}

middleware.antibot = async (req, res, next) => {
	let recaptcha_url = "https://www.google.com/recaptcha/api/siteverify?";
    recaptcha_url += "secret=" + ENV.RECAPTCHA_SECRET_KEY_AUTH + "&";
    recaptcha_url += "response=" + (req.query.token || req.body.token) + "&";
    recaptcha_url += "remoteip=" + req.connection.remoteAddress
	try {
		let response = await Got(recaptcha_url)
		if(!JSON.parse(response.body).success) {
			return res.send({ "message": "Captcha validation failed" });
		}
		else next()
	}
	catch (err) {
		Log.error(err)
	}
}


module.exports = middleware