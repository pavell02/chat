const Crypto 	= require('crypto-random-string')
const Chater	= require('../db/models/chater.js')
const Room	= require('../db/models/room')
const Redis		= require('redis')
const Util = require('util')
const publisher	= Redis.createClient()
const UtilsIo	= require('../io/plugins/utils.js')
const Log		= require('../utils/log')
const CompressImages = require("compress-images")
const Fs 		= require('fs')
const Rename    = Util.promisify(Fs.rename)

publisher.on('error', err => Log.error(err))
.on('connect', () => Log.info('redis publisher connected'))

let chatController = {}

chatController.updloadFile = async (req, res) => {
	const to = req.params.to
	let message = 'Что-то пошло не так'
	try{
		if(!publisher.connected) throw 'Redis is not connected'
		//check
		let invalid, path, chater_id, path_to_file
		switch(to) {
			case 'chat':
				chater_id = req.cookies.chater_id
				let chater = await Chater.findOne({
					chater_id,
					state_chat: 2,
				})
				invalid = !chater_id && chater === null
				path = '/var/www/chat/backend/static/tmp_upload/'
				room_id = chater.room_id
				room = Room.chater_room
				path_to_file = '/static/tmp_upload/'
				break
			case 'conversation':
				invalid = !req.isAuthenticated()
				chater_id = req.user._id
				path = '/var/www/chat/backend/static/media/images/'
				room_id = req.body.room_id
				room = Room.user_room
				path_to_file = '/static/media/images/'
				break
		}
		if(!invalid) {
			let counter = []
			for(let key in req.files) {
				let file = req.files[key]
				let salt = Crypto({length: 35, type: 'url-safe'})
				let fileName = salt + '_' +  file.name.replace(/\s/, '_')
				path += fileName
				file.mv(path,async err => {
					if(err) {
						Log.error(err)
						return res.status(500).json({message})
					}
					CompressImages(path, path, { compress_force: false, autoupdate: false, statistic: false }, false,
						{ jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
						{ png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
						{ svg: { engine: "svgo", command: "--multipass" } },
						{ gif: {engine: 'giflossy', command: ['--lossy=80']}},
						async function (error, completed, statistic) {
							if(error) {
								Log.error(error)
								return res.status(500).json({message})
							}
							try {
								await Rename(statistic.path_out_new, statistic.input)
								let dataMessage = {
									type: 'image',
									name: fileName,
								}
								await UtilsIo.save_message_to_db(dataMessage, room, room_id, chater_id, path_to_file) 
								publisher.publish(to + '_image_' + chater_id, '')
								counter.push(key)
								if(counter.length === Object.keys(req.files).length) {
									return res.json({message: 'Файлы отправлены'})
								}
							}
							catch(err) {
								if(err) {
									Log.error(err)
									return res.status(500).json({message})
								}
							}
						}
					)
				})
			}		
		}
		else {
			return res.status(500).json({
				message: 'Incorrect request'
			})
		}
	}
	catch(err) {
		Log.error(err)
		return res.status(500).json({message})
	}
}

module.exports = chatController