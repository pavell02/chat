const Room	= require('../db/models/room')
const Mongoose = require('mongoose')
const Log = require('../utils/log')
const userRoomController = {}

userRoomController.getRooms = async (req, res) => {
    let model = Room.user_room
    let _id = Mongoose.Types.ObjectId(req.session.passport.user._id)
	let page = parseInt(req.query.page) || 0
	let username = req.query.username
	let limit = parseInt(req.query.limit) || 30
    let rooms = await model.aggregate([
        {
            $match: {
                members: _id,
            }
        },
        {
            $project: {
                members: {
                    $filter: {
                        input: '$members',
                        as: 'member',
                        cond: {$ne: ['$$member', _id]}
                    }
                },
                last_message: {
                    $arrayElemAt: ['$messages', -1]
                },
                'unread_messages': {
                    $size: {
                        "$filter": {
                            "input": {
                                $ifNull: ['$messages', []]
                            },
                            "as": "message",
                            "cond": {
                                $and: [
                                    {
                                        "$eq": [ "$$message.read", false]
                                    },
                                    {
                                        "$ne": ["$$message.whose", _id]
                                    },
                                ]
                            }
                        }
                    }                    
                }
            }
        },
        {
            $sort: {
                'last_message.time': -1,
            }
        },
        {
            $lookup: {
                from: "users",
                localField: 'members',
                foreignField: "_id",
                as: "user"
            }
        },
        {
            $match: {
                'user.username': {
                    $regex: username,
                },
            }
        },
        { 
            $skip : limit * page
        },
        { 
            $limit : limit + 1
        },
        {
            $project: {
                user: {
                    $arrayElemAt: ['$user', 0],
                },
                last_message: '$last_message',
                unread_messages: '$unread_messages',
            }
        }
    ])
    res.json(rooms)
}

userRoomController.count = async (req, res) => {
	let _id = Mongoose.Types.ObjectId(req.session.passport.user._id)
    let model = Room.user_room
    try {
        let count = await model.aggregate([
            {
                $match: {
                    members: _id,
                }
            },
            {
                $project: {
                    last_message: {
                        $arrayElemAt: ['$messages', -1]
                    },
                }
            },
            {
                $match: {
                    'last_message.read': false,
                    'last_message.whose': {
                        $ne: _id,
                    }
                }
            },
            {
                $group: {
                    _id: null,
                    count: {
                        $sum: 1,
                    }
                }
            }
        ])
        count = count.length ? count[0].count : 0
		res.json({count})
    }
    catch(err) {
        Log.error(err)
        res.status(500).json({message: 'smt went wrong'})
    }
}

module.exports = userRoomController