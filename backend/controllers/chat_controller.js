const Chater	= require('../db/models/chater.js')
const Log 		= require('../utils/log')
const chatController = {}

chatController.getRooms = async (req, res) => {
    try {    
        let tags = req.query.tags && JSON.parse(req.query.tags),
        query = [],
        $match = {
            room: true,
            state_chat: 1,
        },
        $skip = parseInt(req.query.skip),
        $limit = parseInt(req.query.limit),
        $sample = {
            size: 25,
        }, 
        $project = {
            tags: "$tags",
            description: "$description",
            "_id": '$_id',
            chater_id: '$chater_id',
        },
        count = 0
        query.push({$match})
        if(tags) {
            $match['tags'] = { $regex: new RegExp(tags.join('|'), 'i') }
            query.push({$skip}, {$limit})
        }
        else {
            query.push({$sample})
        }
        query.push({$project})

        let rooms = await Chater.aggregate(query)
        count = tags ? await Chater.countDocuments($match) : rooms.length
        return res.json({data: rooms, count})
    }
    catch(err) {
        Log.error(err)
    }
}

module.exports = chatController