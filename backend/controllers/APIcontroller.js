const APIController = {}
APIController.exists = async (req, res) => {
	//gettin variables
	let nameDb = req.params['db']
	let db = require(`../db/models/${nameDb}.js`)
	let field = req.params['field']
	let value = req.params['value']
	let canCheck = {}
	let message = 'Что-то пошло не так'
	//able fields for checking
	canCheck['user'] = ['email', 'username']
	//checking
	if(!(canCheck[nameDb] && canCheck[nameDb].includes(field))) {
		return res.json({message})
	}
	//query to db
	try {
		let query = {}
		query[field] = value
		let result = await db.findOne(query).select(field)
		if(result) {
			res.json({exists: true})
		}
		else {
			res.json({exists: false})
		}
	}
	catch (err) {
		console.error(err)
		res.json({message})
	}
}
module.exports = APIController